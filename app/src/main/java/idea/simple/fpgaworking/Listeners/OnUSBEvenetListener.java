package idea.simple.fpgaworking.Listeners;

/**
 * Created by avivvegh on 9/6/15.
 */
public interface OnUSBEvenetListener {

    public void onAck();
    public void onDone();
    public void onError();
    public void onProccess();
}
