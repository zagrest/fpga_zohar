package idea.simple.fpgaworking.Listeners;

/**
 * Created by avivvegh on 10/8/15.
 */
public interface OnNewUSBEventListener {

    public void onMixAck();
    public void onMixDone();
    public void onMixProsses();
    public void onEndStopAck();
    public void onEndStopDone();
    public void onPurgeOnAck();
    public void onPurgeOnDone();
    public void onTempIndicatorOk();
    public void onTempIndicatorError();
    public void onVacuumReleaseAck();
    public void onVacuumReleaseDone();
    public void onCurrectPiston(byte high,byte low);
    public void onFirstPistonPositon(byte high,byte low);


    public void onError(int sel);
    public void onProccess();
}
