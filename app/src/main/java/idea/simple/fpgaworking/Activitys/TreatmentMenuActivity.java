package idea.simple.fpgaworking.Activitys;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import idea.simple.fpgaworking.R;


public class TreatmentMenuActivity extends Activity implements View.OnClickListener {

    ImageView imNext;
    TextView tvMin5, tvMin6,tvMin7,tvMin8,tvMin9, tvMin10,tvNumApplicationDone,tvTODO,tv_title_num_application,tvMinAppText,bUsername;
    ImageView imMinChose5, imMinChose6,imMinChose7,imMinChose8,imMinChose9,imMinChose10,imStopTreat;
    RelativeLayout rlNum5,rlNum6,rlNum7,rlNum8,rlNum9,rlNum10;
    String tosend;
    Typeface fontRobo;
    private boolean check = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().hide();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_treatment_menu);
        fontRobo = Typeface.createFromAsset(getAssets(), "fonts/gothic_bold.ttf");





        init();
    }

    private void init() {
        imStopTreat = (ImageView) findViewById(R.id.imUniqStop);
        imStopTreat.setOnClickListener(this);
        imNext = (ImageView) findViewById(R.id.imTreatNext);
        imNext.setOnClickListener(this);

        bUsername = (TextView) findViewById(R.id.bUsername);
        bUsername.setTypeface(fontRobo);

        tv_title_num_application = (TextView) findViewById(R.id.tv_title_num_application);
        tv_title_num_application.setTypeface(fontRobo);

        tvMinAppText = (TextView) findViewById(R.id.tvMinAppText);
        tvMinAppText.setTypeface(fontRobo);
        tvTODO = (TextView) findViewById(R.id.tvTodo);
        tvTODO.setTypeface(fontRobo);
        tvMin5 = (TextView) findViewById(R.id.tvMin5);
        tvMin5.setTypeface(fontRobo);
        tvMin6 =  (TextView) findViewById(R.id.tvMin6);
        tvMin6.setTypeface(fontRobo);

        tvMin7 = (TextView) findViewById(R.id.tvMin7);
        tvMin7.setTypeface(fontRobo);
        tvMin8 = (TextView) findViewById(R.id.tvMin8);
        tvMin8.setTypeface(fontRobo);

        tvMin9 = (TextView) findViewById(R.id.tvMin9);
        tvMin9.setTypeface(fontRobo);

        tvMin10 = (TextView) findViewById(R.id.tvMin10);
        tvMin10.setTypeface(fontRobo);

        tvNumApplicationDone = (TextView) findViewById(R.id.tvNumApplicationDone);
        tvNumApplicationDone.setTypeface(fontRobo);
        imMinChose5 = (ImageView) findViewById(R.id.imMinChose5);
        imMinChose6 = (ImageView) findViewById(R.id.imMinChose6);
        imMinChose7 = (ImageView) findViewById(R.id.imMinChose7);
        imMinChose8 = (ImageView) findViewById(R.id.imMinChose8);
        imMinChose9 = (ImageView) findViewById(R.id.imMinChose9);
        imMinChose10 = (ImageView) findViewById(R.id.imMinChose10);

        rlNum5 = (RelativeLayout) findViewById(R.id.rlNum5);
        rlNum6 = (RelativeLayout) findViewById(R.id.rlNum6);
        rlNum7 = (RelativeLayout) findViewById(R.id.rlNum7);
        rlNum8 = (RelativeLayout) findViewById(R.id.rlNum8);
        rlNum9 = (RelativeLayout) findViewById(R.id.rlNum9);
        rlNum10 = (RelativeLayout) findViewById(R.id.rlNum10);

        rlNum5.setOnClickListener(this);
        rlNum6.setOnClickListener(this);
        rlNum7.setOnClickListener(this);
        rlNum8.setOnClickListener(this);
        rlNum9.setOnClickListener(this);
        rlNum10.setOnClickListener(this);

        /*
        tvMin5.setOnClickListener(this);
        tvMin6.setOnClickListener(this);
        tvMin7.setOnClickListener(this);
        tvMin8.setOnClickListener(this);
        tvMin9.setOnClickListener(this);
        tvMin10.setOnClickListener(this);

        */
        check_state(check);
    }


    private void check_state(boolean check){
        if(check == false){
            imMinChose5.setVisibility(View.VISIBLE);
            imMinChose6.setVisibility(View.INVISIBLE);
            imMinChose7.setVisibility(View.INVISIBLE);
            imMinChose8.setVisibility(View.INVISIBLE);
            imMinChose9.setVisibility(View.INVISIBLE);
            imMinChose10.setVisibility(View.INVISIBLE);
            tvMin5.setTextColor(Color.parseColor("#0accef"));
            tvMin6.setTextColor(Color.BLACK);
            tvMin7.setTextColor(Color.BLACK);
            tvMin8.setTextColor(Color.BLACK);
            tvMin9.setTextColor(Color.BLACK);
            tvMin10.setTextColor(Color.BLACK);
            this.check = true;
            tosend = "5";
            setCustomTextSize(5);
            funcNum(5);
        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.imUniqStop:
                Intent i = new Intent(TreatmentMenuActivity.this,WelcomeToActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                break;

            case R.id.imTreatNext:
                if (check == true) {
                    Intent toTreatScreen = new Intent(TreatmentMenuActivity.this, TreatmentScreenActivity.class);
                    toTreatScreen.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    if (tosend != null) {
                        toTreatScreen.putExtra("TIME", tosend);
                    }
                    startActivity(toTreatScreen);
                }else{
                    Toast.makeText(getApplicationContext(), "Please chose minutes per application", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.rlNum5:
                imMinChose5.setVisibility(View.VISIBLE);
                imMinChose6.setVisibility(View.INVISIBLE);
                imMinChose7.setVisibility(View.INVISIBLE);
                imMinChose8.setVisibility(View.INVISIBLE);
                imMinChose9.setVisibility(View.INVISIBLE);
                imMinChose10.setVisibility(View.INVISIBLE);
                tvMin5.setTextColor(Color.parseColor("#0accef"));
                tvMin6.setTextColor(Color.BLACK);
                tvMin7.setTextColor(Color.BLACK);
                tvMin8.setTextColor(Color.BLACK);
                tvMin9.setTextColor(Color.BLACK);
                tvMin10.setTextColor(Color.BLACK);
                check = true;
                tosend = "5";
                setCustomTextSize(5);
                funcNum(5);
                break;
            case R.id.rlNum6:
                imMinChose5.setVisibility(View.INVISIBLE);
                imMinChose6.setVisibility(View.VISIBLE);
                imMinChose7.setVisibility(View.INVISIBLE);
                imMinChose8.setVisibility(View.INVISIBLE);
                imMinChose9.setVisibility(View.INVISIBLE);
                imMinChose10.setVisibility(View.INVISIBLE);
                setCustomTextSize(6);

                tosend = "6";
                tvMin6.setTextColor(Color.parseColor("#0accef"));
                tvMin5.setTextColor(Color.BLACK);
                tvMin7.setTextColor(Color.BLACK);
                tvMin8.setTextColor(Color.BLACK);
                tvMin9.setTextColor(Color.BLACK);
                tvMin10.setTextColor(Color.BLACK);
                check = true;

                funcNum(6);

                break;
            case R.id.rlNum7:
                imMinChose5.setVisibility(View.INVISIBLE);
                imMinChose6.setVisibility(View.INVISIBLE);
                imMinChose7.setVisibility(View.VISIBLE);
                imMinChose8.setVisibility(View.INVISIBLE);
                imMinChose9.setVisibility(View.INVISIBLE);
                imMinChose10.setVisibility(View.INVISIBLE);
                setCustomTextSize(7);


                tosend = "7";
                tvMin7.setTextColor(Color.parseColor("#0accef"));
                tvMin6.setTextColor(Color.BLACK);
                tvMin5.setTextColor(Color.BLACK);
                tvMin8.setTextColor(Color.BLACK);
                tvMin9.setTextColor(Color.BLACK);
                tvMin10.setTextColor(Color.BLACK);
                funcNum(7);
                check = true;

                break;
            case R.id.rlNum8:
                imMinChose5.setVisibility(View.INVISIBLE);
                imMinChose6.setVisibility(View.INVISIBLE);
                imMinChose7.setVisibility(View.INVISIBLE);
                imMinChose8.setVisibility(View.VISIBLE);
                imMinChose9.setVisibility(View.INVISIBLE);
                imMinChose10.setVisibility(View.INVISIBLE);
                tosend = "8";
                tvMin8.setTextColor(Color.parseColor("#0accef"));
                tvMin6.setTextColor(Color.BLACK);
                tvMin7.setTextColor(Color.BLACK);
                tvMin5.setTextColor(Color.BLACK);
                tvMin9.setTextColor(Color.BLACK);
                tvMin10.setTextColor(Color.BLACK);
                setCustomTextSize(8);

                funcNum(8);
                check = true;

                break;
            case R.id.rlNum9:
                imMinChose5.setVisibility(View.INVISIBLE);
                imMinChose6.setVisibility(View.INVISIBLE);
                imMinChose7.setVisibility(View.INVISIBLE);
                imMinChose8.setVisibility(View.INVISIBLE);
                imMinChose9.setVisibility(View.VISIBLE);
                imMinChose10.setVisibility(View.INVISIBLE);
                tosend = "9";
                tvMin9.setTextColor(Color.parseColor("#0accef"));
                setCustomTextSize(9);

                tvMin6.setTextColor(Color.BLACK);
                tvMin7.setTextColor(Color.BLACK);
                tvMin8.setTextColor(Color.BLACK);
                tvMin5.setTextColor(Color.BLACK);
                tvMin10.setTextColor(Color.BLACK);
                funcNum(9);
                check = true;

                break;
            case R.id.rlNum10:
                imMinChose5.setVisibility(View.INVISIBLE);
                imMinChose6.setVisibility(View.INVISIBLE);
                imMinChose7.setVisibility(View.INVISIBLE);
                imMinChose8.setVisibility(View.INVISIBLE);
                imMinChose9.setVisibility(View.INVISIBLE);
                imMinChose10.setVisibility(View.VISIBLE);
                tosend = "10";
                tvMin10.setTextColor(Color.parseColor("#0accef"));
                tvMin6.setTextColor(Color.BLACK);
                tvMin7.setTextColor(Color.BLACK);
                tvMin8.setTextColor(Color.BLACK);
                tvMin9.setTextColor(Color.BLACK);
                tvMin5.setTextColor(Color.BLACK);
                setCustomTextSize(10);

                funcNum(10);
                check = true;

                break;


        }


    }

    private void setCustomTextSize(int num){
        switch (num){
            case 5:
                tvMin5.setTextSize(40);
                tvMin6.setTextSize(35);
                tvMin7.setTextSize(35);
                tvMin8.setTextSize(35);
                tvMin9.setTextSize(35);
                tvMin10.setTextSize(35);


                break;
            case 6:
                tvMin5.setTextSize(35);
                tvMin6.setTextSize(40);
                tvMin7.setTextSize(35);
                tvMin8.setTextSize(35);
                tvMin9.setTextSize(35);
                tvMin10.setTextSize(35);
                break;
            case 7:
                tvMin5.setTextSize(35);
                tvMin6.setTextSize(35);
                tvMin7.setTextSize(40);
                tvMin8.setTextSize(35);
                tvMin9.setTextSize(35);
                tvMin10.setTextSize(35);
                break;
            case 8:
                tvMin5.setTextSize(35);
                tvMin6.setTextSize(35);
                tvMin7.setTextSize(35);
                tvMin8.setTextSize(40);
                tvMin9.setTextSize(35);
                tvMin10.setTextSize(35);
                break;
            case 9:
                tvMin5.setTextSize(35);
                tvMin6.setTextSize(35);
                tvMin7.setTextSize(35);
                tvMin8.setTextSize(35);
                tvMin9.setTextSize(40);
                tvMin10.setTextSize(35);
                break;
            case 10:
                tvMin5.setTextSize(35);
                tvMin6.setTextSize(35);
                tvMin7.setTextSize(35);
                tvMin8.setTextSize(35);
                tvMin9.setTextSize(35);
                tvMin10.setTextSize(40);
                break;
        }

    }

    private void funcNum(int min){
        switch (min){
            case 5:
                tvNumApplicationDone.setText("11");
                break;
            case 6:
                tvNumApplicationDone.setText("10");
                break;
            case 7:
                tvNumApplicationDone.setText("9");
                break;
            case 8:
                tvNumApplicationDone.setText("8");
                break;
            case 9:
                tvNumApplicationDone.setText("7");
                break;
            case 10:
                tvNumApplicationDone.setText("6");
                break;
        }
    }
}
