package idea.simple.fpgaworking.Activitys;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import idea.simple.fpgaworking.R;


public class EndSessionActivity extends Activity {

    ImageView imEsNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_session);
        getActionBar().hide();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        imEsNext = (ImageView) findViewById(R.id.imEsNext);
        imEsNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toWelcome = new Intent(EndSessionActivity.this,WelcomeToActivity.class);
                toWelcome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(toWelcome);
            }
        });
    }


}
