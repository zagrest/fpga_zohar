package idea.simple.fpgaworking.Activitys;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import idea.simple.fpgaworking.Comboseekbar.ComboSeekBar;
import idea.simple.fpgaworking.CustomViews.ProgressView;
import idea.simple.fpgaworking.Handlers.LoadTimer;
import idea.simple.fpgaworking.R;

public class TreatmentScreenActivity extends Activity implements View.OnClickListener, LoadTimer.LoadingTaskFinishedListener {

    ImageView imTreatStop, imState0,imState1,imState2,imState3,imState4,imState5,imState6,imState7 ;
    int state;
    Button bSwitch1,bSwitch2,bSwitch3,bSwitch4,bSwitch5,bSwitch6;
    MyCount count;
    long counter =600000;
    private ProgressView mProgress;
    String hms,tms;
    TextView tvTimeLeft,tvTimeStated,bUsername,tvTimeToFinish,tvTextTimeStart,tvHigh,tvMid,tvLow, tvTemp,tvVacuum;
    TextView tvNum1,tvNum2,tvNum3,tvNum4,tvNum5,tvNum6,tvNum7,tvNum8;
    ImageView imTreatNum;
    LoadTimer loader;
    Typeface fontRobo;
    String time;
    Timer timer;
    Timer T;
    ComboSeekBar mSeekBar,mSeekVacuum;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_treatment_screen);
        getActionBar().hide();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        fontRobo = Typeface.createFromAsset(getAssets(), "fonts/gothic.ttf");

        Bundle extras = getIntent().getExtras();


        if (extras != null) {
            time = extras.getString("TIME");

        }

        state = 4;
        init();
        checkState(4);
        if (time !=null)
            getTime(time);




    }

    private void checkState(int num){
        switch(num){
            case 0:
                imTreatNum.setImageResource(R.drawable.treatnumone);
//                tvNum1.setTextColor(Color.parseColor("#0accef"));
                break;
            case 1:
                imTreatNum.setImageResource(R.drawable.treatnumtwo);
//                tvNum2.setTextColor(Color.parseColor("#0accef"));

                break;
            case 2:
                imTreatNum.setImageResource(R.drawable.treatnumthree);
//                tvNum3.setTextColor(Color.parseColor("#0accef"));
                break;
            case 3:
                imTreatNum.setImageResource(R.drawable.treatnumfour);
//                tvNum4.setTextColor(Color.parseColor("#0accef"));

                break;
            case 4:
                imTreatNum.setImageResource(R.drawable.treatnumfive);
//                tvNum5.setTextColor(Color.parseColor("#0accef"));

                break;
            case 5:
                imTreatNum.setImageResource(R.drawable.treatnumsix);
//                tvNum6.setTextColor(Color.parseColor("#0accef"));

                break;
            case 6:
                imTreatNum.setImageResource(R.drawable.treatnumseven);
//                tvNum7.setTextColor(Color.parseColor("#0accef"));

                break;
            case 7:
                imTreatNum.setImageResource(R.drawable.treatnumeate);
//                tvNum8.setTextColor(Color.parseColor("#0accef"));

                break;

        }
    }

    public void getTime(String time){
        switch (Integer.valueOf(time)){
            case 5:
                tvTimeStated.setText("05:00");
                counter = 300000;
                break;
            case 6:
                tvTimeStated.setText("06:00");
                counter = 360000;

                break;
            case 7:
                tvTimeStated.setText("07:00");
                counter = 420000;

                break;
            case 8:
                tvTimeStated.setText("08:00");
                counter = 480000;

                break;
            case 9:
                tvTimeStated.setText("09:00");
                counter = 540000;

                break;
            case 10:
                tvTimeStated.setText("10:00");
                counter = 600000;

                break;
        }
    }

    private void init() {
        tvTemp = (TextView) findViewById(R.id.tvTemp);
        tvTemp.setTypeface(fontRobo);
        tvVacuum = (TextView) findViewById(R.id.tvVacuum);
        tvVacuum.setTypeface(fontRobo);

        tvHigh = (TextView) findViewById(R.id.tvHigh);
        tvHigh.setTypeface(fontRobo);
        tvMid =(TextView) findViewById(R.id.tvMid);
        tvMid.setTypeface(fontRobo);
        tvLow = (TextView) findViewById(R.id.tvLow);
        tvLow.setTypeface(fontRobo);
        tvTextTimeStart = (TextView) findViewById(R.id.tvTextTimeStart);
        tvTextTimeStart.setTypeface(fontRobo);
        tvTimeToFinish =(TextView) findViewById(R.id.tvTimeToFinish);
        tvTimeToFinish.setTypeface(fontRobo);
        bUsername = (TextView) findViewById(R.id.bUsername);
        bUsername.setTypeface(fontRobo);
        imTreatStop = (ImageView) findViewById(R.id.imTreatStop);
        imTreatStop.setOnClickListener(this);
        mProgress = (ProgressView) findViewById(R.id.circle_progress_bar);
        mProgress.setFirstTitle("TIME TO FINISH:");
        mProgress.setSecondTitle("TIME FROM START:");
        mProgress.setMax(300);//Total timer duration
        mProgress.setProgress(247);//Starting progress

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mProgress.setProgress(mProgress.getProgress() + 1f);
                    }
                });
            }
        }, 0, 1000);

//        mProgress.setProgress(100);
//        loader = new LoadTimer(mProgress, this,60000);
//        loader.execute("");

        tvTimeLeft = (TextView) findViewById(R.id.tvTimeLeft);
        tvTimeLeft.setTypeface(fontRobo);
        tvTimeStated = (TextView) findViewById(R.id.tvTimeStated);
        tvTimeStated.setTypeface(fontRobo);


        T=new Timer();
        T.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //int seconds = counter % 60;
                        int seconds = (int) (counter / 1000) % 60;

                        int minutes = (int) ((counter / (1000)) / 60);

//                        int minutes = (counter / 60) % 60;
                        tvTimeStated.setText(String.format("%02d:%02d", minutes, seconds));
                        counter += 1000;
                    }
                });

            }
        }, 0, 1000);

        int set1 = 60000;
        long millisUntilFinished1 = set1;
        long millis1 = millisUntilFinished1;
        hms = String.format("%02d:%02d",
                (int) ((millis1 / (1000 * 60)) % 60),
                (int) (millis1 / 1000) % 60);
        tvTimeLeft.setText(hms);

        count = new MyCount(60000,1000);
        count.start();


//        imState0 = (ImageView) findViewById(R.id.imState0);
//        imState1 = (ImageView) findViewById(R.id.imState1);
//        imState2 = (ImageView) findViewById(R.id.imState2);
//        imState3 = (ImageView) findViewById(R.id.imState3);
//        imState4 = (ImageView) findViewById(R.id.imState4);
//        imState5 = (ImageView) findViewById(R.id.imState5);
//        imState6 = (ImageView) findViewById(R.id.imState6);
//        imState7 = (ImageView) findViewById(R.id.imState7);

        imTreatNum = (ImageView) findViewById(R.id.imTreatNum);
        //        bSwitch1 = (Button) findViewById(R.id.bSwitch1);
//        bSwitch2 = (Button) findViewById(R.id.bSwitch2);
//        bSwitch3 = (Button) findViewById(R.id.bSwitch3);
//        bSwitch4 = (Button) findViewById(R.id.bSwitch4);
//        bSwitch5 = (Button) findViewById(R.id.bSwitch5);
//        bSwitch6 = (Button) findViewById(R.id.bSwitch6);

//        tvNum1 = (TextView) findViewById(R.id.tvNum1);
//        tvNum1.setTypeface(fontRobo);
//        tvNum2 = (TextView) findViewById(R.id.tvNum2);
//        tvNum2.setTypeface(fontRobo);
//        tvNum3 = (TextView) findViewById(R.id.tvNum3);
//        tvNum3.setTypeface(fontRobo);
//        tvNum4 = (TextView) findViewById(R.id.tvNum4);
//        tvNum4.setTypeface(fontRobo);
//        tvNum5 =(TextView) findViewById(R.id.tvNum5);
//        tvNum5.setTypeface(fontRobo);
//        tvNum6 = (TextView) findViewById(R.id.tvNum6);
//        tvNum6.setTypeface(fontRobo);
//        tvNum7 = (TextView) findViewById(R.id.tvNum7);
//        tvNum7.setTypeface(fontRobo);
//        tvNum8 = (TextView) findViewById(R.id.tvNum8);
//        tvNum8.setTypeface(fontRobo);
//
//        bSwitch1.setOnClickListener(this);
//        bSwitch2.setOnClickListener(this);
//        bSwitch3.setOnClickListener(this);
//        bSwitch4.setOnClickListener(this);
//        bSwitch5.setOnClickListener(this);
//        bSwitch6.setOnClickListener(this);

        mSeekBar = (ComboSeekBar) findViewById(R.id.seekBar2);
        List<String> seekBarStep = Arrays.asList("", "", "");
        mSeekBar.setAdapter(seekBarStep);

        mSeekBar.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                switch (position) {
                    case 0:

                        //do something here
                        break;
                    case 1:
                        //do something here
                        break;
                    case 2:
                        //do something here
                        break;
                }

            }
        });



        mSeekVacuum = (ComboSeekBar) findViewById(R.id.seekBar3);
        List<String> seekBarVacuum = Arrays.asList("", "", "");
        mSeekVacuum.setAdapter(seekBarStep);

        mSeekVacuum.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                switch (position) {
                    case 0:

                        //do something here
                        break;
                    case 1:
                        //do something here
                        break;
                    case 2:
                        //do something here
                        break;
                }

            }
        });


    }




    @Override
    public void onClick(View v) {
        switch (v.getId()){

//            case R.id.bSwitch1:
//                bSwitch1.setBackgroundResource(R.drawable.treathigh);
//                bSwitch2.setBackgroundResource(R.drawable.treatmidgrey);
//                bSwitch3.setBackgroundResource(R.drawable.treatlowgrey);
//                break;
//            case R.id.bSwitch2:
//                bSwitch1.setBackgroundResource(R.drawable.treathighgrey);
//                bSwitch2.setBackgroundResource(R.drawable.treatmid);
//                bSwitch3.setBackgroundResource(R.drawable.treatlowgrey);
//
//                break;
//            case R.id.bSwitch3:
//                bSwitch1.setBackgroundResource(R.drawable.treathighgrey);
//                bSwitch2.setBackgroundResource(R.drawable.treatmidgrey);
//                bSwitch3.setBackgroundResource(R.drawable.treatlow);
//                break;
//            case R.id.bSwitch4:
//                bSwitch4.setBackgroundResource(R.drawable.treathigh);
//                bSwitch5.setBackgroundResource(R.drawable.treatmidgrey);
//                bSwitch6.setBackgroundResource(R.drawable.treatlowgrey);
//                break;
//            case R.id.bSwitch5:
//                bSwitch4.setBackgroundResource(R.drawable.treathighgrey);
//                bSwitch5.setBackgroundResource(R.drawable.treatmid);
//                bSwitch6.setBackgroundResource(R.drawable.treatlowgrey);
//                break;
//            case R.id.bSwitch6:
//                bSwitch4.setBackgroundResource(R.drawable.treathighgrey);
//                bSwitch5.setBackgroundResource(R.drawable.treatmidgrey);
//                bSwitch6.setBackgroundResource(R.drawable.treatlow);
//                break;


            case R.id.imTreatStop:
                Intent toPurge = new Intent(TreatmentScreenActivity.this, PurgeActivity.class);
                toPurge.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(toPurge);
//                loader.cancel(true);
                timer.cancel();
                T.cancel();
                finish();
                break;
        }

    }

    @Override
    public void onTaskFinsihed() {

    }

    public class MyCount extends CountDownTimer {
        public MyCount(long millisFuture, long countdownInterval){
            super(millisFuture,countdownInterval);

        }
        @Override
        public void onTick(long millisUntilFinished) {
            long millis3 = millisUntilFinished;
            hms = String.format("%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(millis3) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis3)),
                    TimeUnit.MILLISECONDS.toSeconds(millis3) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis3)));
            tvTimeLeft.setText(hms);

        }

        @Override
        public void onFinish() {
//            tvTimeLeft.setText("00:00");
            T.cancel();
            timer.cancel();
        }
    }


}
