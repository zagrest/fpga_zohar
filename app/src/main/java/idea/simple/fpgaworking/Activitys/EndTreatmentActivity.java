package idea.simple.fpgaworking.Activitys;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import idea.simple.fpgaworking.R;


public class EndTreatmentActivity extends Activity {

    CheckBox cbRemoveMpu, cbRinseMounth, cbRinseMPU, cbPlaceMpuOnTry, cbInspectSoftTissue;
    ImageView imCheckall;
    ImageView imTreatStop;
    Typeface fontRobo;
    TextView bUsername,tvRemoveMpu,tvCheckall,tvEnd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_treatment);
        getActionBar().hide();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        fontRobo = Typeface.createFromAsset(getAssets(), "fonts/gothic.ttf");


        init();
        cbRemoveMpu = (CheckBox) findViewById(R.id.cbRemoveMpu);
        cbRemoveMpu.setTypeface(fontRobo);
        cbRinseMounth = (CheckBox) findViewById(R.id.cbRinseMounth);
        cbRinseMounth.setTypeface(fontRobo);
        cbRinseMPU = (CheckBox) findViewById(R.id.cbRinseMPU);
        cbRinseMPU.setTypeface(fontRobo);
        cbPlaceMpuOnTry = (CheckBox) findViewById(R.id.cbPlaceMpuOnTry);
        cbPlaceMpuOnTry.setTypeface(fontRobo);
        cbInspectSoftTissue = (CheckBox) findViewById(R.id.cbInspectSoftTissue);
        cbInspectSoftTissue.setTypeface(fontRobo);

        imCheckall = (ImageView) findViewById(R.id.imCheckall);
        imTreatStop = (ImageView) findViewById(R.id.imTreatStop);
        imCheckall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cbRemoveMpu.setChecked(true);
                cbRinseMounth.setChecked(true);
                cbRinseMPU.setChecked(true);
                cbPlaceMpuOnTry.setChecked(true);
                cbInspectSoftTissue.setChecked(true);
            }
        });


        imTreatStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(EndTreatmentActivity.this, EndSessionActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });


    }

    private void init() {
        bUsername = (TextView) findViewById(R.id.bUsername);
        bUsername.setTypeface(fontRobo);
        tvRemoveMpu = (TextView) findViewById(R.id.tvRemoveMpu);
        tvRemoveMpu.setTypeface(fontRobo);
        tvCheckall =(TextView) findViewById(R.id.tvCheckall);
        tvCheckall.setTypeface(fontRobo);
        tvEnd = (TextView) findViewById(R.id.tvEnd);
        tvEnd.setTypeface(fontRobo);
    }


}
