package idea.simple.fpgaworking.Activitys;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import idea.simple.fpgaworking.Constance.Const;
import idea.simple.fpgaworking.FPGACommunicator.FPGACommucnicator_new;
import idea.simple.fpgaworking.Listeners.OnNewUSBEventListener;
import idea.simple.fpgaworking.NewFragments.MixingFragment_new;
import idea.simple.fpgaworking.NewFragments.PurgeFragment_new;
import idea.simple.fpgaworking.NewFragments.SplashFragment_new;
import idea.simple.fpgaworking.NewFragments.TreatMenuFragment_new;
import idea.simple.fpgaworking.NewFragments.TreatScreenFragment_new_new;
import idea.simple.fpgaworking.R;

public class MainNew extends ActionBarActivity {

    FrameLayout frameLayout;
    FPGACommucnicator_new communicator;
    Const con;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    int A_MAX;
    int DELTA_Q;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().hide();
        setContentView(R.layout.activity_main_new);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        con = new Const();
        prefs = getSharedPreferences(con.MY_PREFS_NAME, MODE_PRIVATE);
        editor = getSharedPreferences(con.MY_PREFS_NAME, MODE_PRIVATE).edit();

        A_MAX = prefs.getInt(con.A_MAX_NUMBER,0);
        DELTA_Q = 0;
        OnNewUSBEventListener listener = new OnNewUSBEventListener() {
            @Override
            public void onMixAck() {


            }

            @Override
            public void onMixDone() {
                MixingFragment_new fragt = (MixingFragment_new) getFragmentManager().findFragmentByTag(con.MIX_TAG);
                if (fragt !=null) {
                    fragt.toTreatMenu();
                    Log.v("MainNew","MIX");
                }
            }

            @Override
            public void onMixProsses() {
                MixingFragment_new fragt = (MixingFragment_new) getFragmentManager().findFragmentByTag(con.MIX_TAG);
                if (fragt !=null) {
                    fragt.ErrToast();

                }


            }

            @Override
            public void onEndStopAck() {
                TreatMenuFragment_new fragt = (TreatMenuFragment_new) getFragmentManager().findFragmentByTag(con.TREAT_MENU_TAG);
                fragt.toWelcome();
            }

            @Override
            public void onEndStopDone() {
//                TreatMenuFragment_new fragt = (TreatMenuFragment_new) getFragmentManager().findFragmentByTag(con.TREAT_MENU_TAG);
//                fragt.toWelcome();
            }

            @Override
            public void onPurgeOnAck() {
                TreatScreenFragment_new_new fragt = (TreatScreenFragment_new_new) getFragmentManager().findFragmentByTag(con.TREAT_SCREEN_TAG);
                fragt.goToPurge();

            }

            @Override
            public void onPurgeOnDone() {

            }

            @Override
            public void onTempIndicatorOk() {
                TreatScreenFragment_new_new fragt = (TreatScreenFragment_new_new) getFragmentManager().findFragmentByTag(con.TREAT_SCREEN_TAG);
                fragt.temoIndicatorProsses();
            }

            @Override
            public void onTempIndicatorError() {
                TreatScreenFragment_new_new fragt = (TreatScreenFragment_new_new) getFragmentManager().findFragmentByTag(con.TREAT_SCREEN_TAG);
                fragt.tempIndicatorError();
            }

            @Override
            public void onVacuumReleaseAck() {
                PurgeFragment_new fragt = (PurgeFragment_new) getFragmentManager().findFragmentByTag(con.PURGE_TAG);
                fragt.toTreatMenu();
            }

            @Override
            public void onVacuumReleaseDone() {
                PurgeFragment_new fragt = (PurgeFragment_new) getFragmentManager().findFragmentByTag(con.PURGE_TAG);
                fragt.toTreatMenu();
            }

            @Override
            public void onCurrectPiston(byte high, byte low) {
                TreatScreenFragment_new_new fragt = (TreatScreenFragment_new_new) getFragmentManager().findFragmentByTag(con.TREAT_SCREEN_TAG);
                fragt.setPistonUi(high, low);
            }
            final int MIX_FRAME = 0;
            final int TREAT_FRAME = 1;
            final int WELCOME_FRAME = 2;
            final int PURGE_FRAME = 3;

            @Override
            public void onError(int sel){
                // error dealing func
                switch (sel)
                {
                    case MIX_FRAME:
                        MixingFragment_new fragt = (MixingFragment_new) getFragmentManager().findFragmentByTag(con.MIX_TAG);
                        if (fragt !=null) {
                            fragt.ErrToast();
                        }
                        break;
                    case TREAT_FRAME:
                        TreatMenuFragment_new fragtT = (TreatMenuFragment_new) getFragmentManager().findFragmentByTag(con.TREAT_MENU_TAG);
                        fragtT.OnError();
                        break;
                    case  WELCOME_FRAME:
                        TreatMenuFragment_new fragtw = (TreatMenuFragment_new) getFragmentManager().findFragmentByTag(con.TREAT_MENU_TAG);
                        fragtw.toWelcome_err();

                        break;
                    case  PURGE_FRAME:
                        MixingFragment_new fragtpurg = (MixingFragment_new) getFragmentManager().findFragmentByTag(con.MIX_TAG);
                        if (fragtpurg !=null) {
                            fragtpurg.ErrToast();
                        }

                        break;
                    default:
                        break;
                }
            }
            @Override
            public void onFirstPistonPositon(byte high, byte low) {
                double num = 0.0;
                //do and to high and low with 256 ( high & 0xFF)
                num = unsignedByteToInt(high) * 256;
                num += unsignedByteToInt(low);

                //multiply the calculate num with 0.1364223
                num = num * 0.1364223;

                //convert the number to int
                int number = (int) num;

                //calculating DELTA_Q
                DELTA_Q = A_MAX - number;

//                (1-(number-number)/DELTA_Q)*100
                TreatScreenFragment_new_new fragt = (TreatScreenFragment_new_new) getFragmentManager().findFragmentByTag(con.TREAT_SCREEN_TAG);

                //send DELTA_Q to TREATMENT SCREEN
                if (fragt!=null) fragt.setPositon_min(number,DELTA_Q);

                editor.putInt(con.DELTA_Q,DELTA_Q);
                //Put DELTA_Q as the new first time minimum
                editor.putInt(con.PISTION_MIN, DELTA_Q);
                editor.commit();

                Log.v("POSITION LOG", "PISTON MAIN");

            }


            @Override
            public void onProccess() {

            }
        };
        communicator = new FPGACommucnicator_new(getApplicationContext(),listener);

        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        SplashFragment_new fragment = new SplashFragment_new(communicator);
        fragmentTransaction.add(R.id.frameLayout, fragment, con.SPLASH_TAG);
        fragmentTransaction.commit();

//        Timer treatTimer = new Timer();
//        treatTimer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        WelcomeFragment_new fragt = (WelcomeFragment_new) getFragmentManager().findFragmentByTag(con.WELCOME_TAG);
//                        if (fragt != null) {
//                            fragt.toast();
//
//                        }
//                    }
//                });
//            }
//        }, 3000, 10000);


//
//        WelcomeFragment_new fragt = (WelcomeFragment_new) getFragmentManager().findFragmentByTag("WELCOM_FRAG");
//        fragt.toast();

//        SplashFragment_new splash = new SplashFragment_new(communicator);


//        SplashFragment_new fragment = (SplashFragment_new) getFragmentManager().findFragmentById(R.id.menufrag);
//        fragment.specific_function_name();


    }

    public static int unsignedByteToInt(byte b) {
        return (int) b & 0xFF;
    }




}
