package idea.simple.fpgaworking.Activitys;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;

import idea.simple.fpgaworking.R;


public class SplashActivity extends Activity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getActionBar().hide();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//        Intent intent = new Intent(getApplicationContext(), MainService.class);
////        startService(intent);
//        bindService(intent, this, 0);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);//zohar 3000

                    // Do some stuff
                } catch (Exception e) {
                    e.getLocalizedMessage();
                } finally {
                    Intent toMain = new Intent(SplashActivity.this, WelcomeToActivity.class);
                    startActivity(toMain);
                    finish();
                }
            }
        }).start();
    }


}
