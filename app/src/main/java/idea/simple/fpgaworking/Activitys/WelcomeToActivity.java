package idea.simple.fpgaworking.Activitys;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import idea.simple.fpgaworking.R;


public class WelcomeToActivity extends Activity implements View.OnClickListener {

    ImageView imWhite, imPatientScreen;
    Typeface fontRobo;
    TextView tv_welcome,tv_white_treat,tv_patient_screening;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_to);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getActionBar().hide();
        fontRobo = Typeface.createFromAsset(getAssets(), "fonts/gothic.ttf");

        init();


    }

    private void init() {
        tv_patient_screening= (TextView) findViewById(R.id.tv_patient_screening);
        tv_patient_screening.setTypeface(fontRobo);
        tv_white_treat = (TextView) findViewById(R.id.tv_white_treat);
        tv_white_treat.setTypeface(fontRobo);

        tv_welcome = (TextView) findViewById(R.id.tv_welcome);
        tv_welcome.setTypeface(fontRobo);
        imWhite = (ImageView) findViewById(R.id.im_product1);
        imPatientScreen = (ImageView) findViewById(R.id.im_product2);

        imWhite.setOnClickListener(this);
        imPatientScreen.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.im_product1):
                Intent toMix = new Intent(WelcomeToActivity.this, MixingActivity.class);
                toMix.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(toMix);
                break;
            case (R.id.im_product2):
                break;
        }
    }
}
