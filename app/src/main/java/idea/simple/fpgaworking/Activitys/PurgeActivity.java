package idea.simple.fpgaworking.Activitys;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.TextView;

import idea.simple.fpgaworking.R;


public class PurgeActivity extends Activity
{

    private int progressStatus = 0;
    private int progress = 0;
    SeekBar pbSeek;

    private Handler handler = new Handler();
    TextView tvProgressText,tvPurgeWarning;
    Typeface fontRobo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purge);
        getActionBar().hide();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        fontRobo = Typeface.createFromAsset(getAssets(), "fonts/gothic.ttf");
        tvPurgeWarning = (TextView) findViewById(R.id.tvPurgeWarning);
        tvPurgeWarning.setTypeface(fontRobo);
        tvProgressText = (TextView) findViewById(R.id.tvProgressText);
        pbSeek = (SeekBar) findViewById(R.id.pbSeekbar);
        pbSeek.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

       // new LoadingTask(pbProgress,this).execute("");
//        MyCount count = new MyCount(30000,1000);
        //count.start();
        new Thread(new Runnable() {
            public void run() {
                while (progressStatus < 100) {
                    progressStatus += 1;
                    // Update the progress bar and display the
                    //current value in the text view
                    handler.post(new Runnable() {
                        public void run() {
                            tvProgressText.setText("PURGE " + progressStatus +"%");
                            pbSeek.setProgress(progressStatus);
                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        //Just to display the progress slowly
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                Intent toMain = new Intent(PurgeActivity.this, TreatmentMenuActivity.class);
                startActivity(toMain);
                finish();

            }
        }).start();
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    Thread.sleep(3000);
//
//                    // Do some stuff
//                } catch (Exception e) {
//                    e.getLocalizedMessage();
//                } finally {
//                    Intent toMain = new Intent(PurgeActivity.this, EndTreatmentActivity.class);
//                    startActivity(toMain);
//                    finish();
//                }
//            }
//        }).start();


    }
    public class MyCount extends CountDownTimer {
        public MyCount(long millisFuture, long countdownInterval) {
            super(millisFuture, countdownInterval);

        }

        @Override
        public void onTick(long millisUntilFinished) {
            long millis3 = millisUntilFinished;
            progressStatus += 1;
            progress +=3;
            pbSeek.setProgress(progress);
            tvProgressText.setText("PURGE " + progressStatus + "%");

        }

        @Override
        public void onFinish() {
//            tvTimeStated.setText("11:00");
            Intent toMain = new Intent(PurgeActivity.this, TreatmentMenuActivity.class);
            startActivity(toMain);
            finish();
        }
    }



}
