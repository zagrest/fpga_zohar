package idea.simple.fpgaworking.Constance;

/**
 * Created by avivvegh on 9/3/15.
 */
public class Const {


    public byte INDEX = 0x01; //will increment every function
    public byte BITHIGH = 0x01;
    public byte BITLOW = 0x01;

    public byte VACUUM_HIGH_BITHIGH = 11;
    public byte VACUUM_HIGH_BITLOW = (byte) 238;

    public  byte VACUUM_MID_BITHIGH = 12;
    public  byte VACUUM_MID_BITLOW = (byte) 150;


//    public static byte VACUUM_LOW_BITHIGH = 0x0C;
//    public static byte VACUUM_LOW_BITLOW = (byte) 0x8C;
    public  byte VACUUM_LOW_BITHIGH = (byte) 140;
    public  byte VACUUM_LOW_BITLOW = 12;

    public  byte VACUUM_BITHIGH;
    public  byte VACUUM_BITLOW;
//    public static byte[] SETTING_INIT = {INDEX,0x01,0x02,0x0,0x0,0x0};
    public  byte[] COUNTA = {INDEX,0x01,0x02,0x1,0x0,0x2D};



    public  byte[] GAS_RELEAS = {INDEX,0x01,0x02,0x02,0x0,0x0};
    public  byte[] A_MAX = {INDEX,0x01,0x02,0x03,0x0,(byte) 219};//0x2,0xac zohar
    public  byte[] B_MAX = {INDEX,0x01,0x02,0x04,0x0,(byte) 219};//0xac zohar
    public  byte[] A_B_MIN = {INDEX,0x01,0x02,0x06,0x0,0x44};
    public  byte[] MIX_TIME = {INDEX,0x01,0x02,0x07,0x0,0x02};

    public  byte[] MIXING = {INDEX,0x01,0x03,0x0,0x0,0x0};//byte 5=0 zohar

    public  byte[] BLADE_LOCK = {INDEX,0x01,12,0x0,0x0, (byte) 240};
    public  byte[] BLADE_OPEN = {INDEX,0x01,12,0x0,0x0, (byte) 22};


    public  byte[] TREATMENT = {INDEX,0x01,0x01,0x02,0x0,0x0};
    public  byte[] SPECIAL_COUNTA = {INDEX,0x01,0x02,0x1,1, 81};

    public  byte[] TREATMENT_COUNTINUE = {INDEX,0x01,0x01,0x02,0x0,0x1};

    //Temp !

    public  byte[] TEMP_INDICATOR = {INDEX,0x02,0x03,0x0,0x0,0x0};
    public  byte[] TEMP_INDICATOR_OK = {INDEX,0x02,0x03,0x0,0x0,0x1};



    //public static byte[] TEMP_CHECK = {INDEX,0x01,0x03,0x01,0x0,0x0};
    public  byte[] VACUUM_ON = {INDEX,0x01,0x06,0x01, VACUUM_BITHIGH, VACUUM_BITLOW};
    public  byte[] VACUUM_OFF = {INDEX,0x01,0x06,0x00, VACUUM_BITHIGH, VACUUM_BITLOW};

    //valve opcode
    public  byte[] VALVE_MIX = {INDEX,0x01,0x0A,0, 0 , (byte) 245};
    public  byte[] VALVE = {INDEX,0x01,0x0A,0, 9 , 112};

    public  byte[] VAC_RELEASE = {INDEX,0x01,0x0A,0, 6 , 82};


    public  byte[] VACUUM_ON_LOW = {INDEX,0x01,0x06,0x01, (byte) 12, (byte) 140};
    public  byte[] VACUUM_ON_MID = {INDEX,0x01,0x06,0x01, (byte) 12, (byte) 50};
    public  byte[] VACUUM_ON_HIGH = {INDEX,0x01,0x06,0x01,  11, (byte) 238};
    public  byte[] PURGE_ON = {INDEX,0x01,0x06,0x01,10, (byte) 202};
    public  byte[] PURGE_OFF = {INDEX,0x01,0x06,0x00,12, 50};

    //    public static byte[] VACCUM_RELEASE = {INDEX,0x01,0x0A,0x0,0x0,0x0};
    public  byte[] VACCUM_RELEASE = {INDEX,0x01,0x01,0x04,VACUUM_BITHIGH,VACUUM_BITLOW};

    public  byte[] END_TREATMENT = {INDEX,0x01,0x01,0x03,0x0,0x0};
    public  byte[] END_STOP = {INDEX,0x01,0x07,0x0,0x0,0x0};

    public  byte[] PISTON_CURRENT_PLACE = {0,0x02,0x0A,0x0,0x0,0x0};

    public  byte[] PISTON_CURRENT_PLACE_FIRST = {1,0x02,0x0A,0x0,0x0,0x0};

    public  byte[] RESET_MSG = {0x11,0x02,0x07,0x0,0x0,0x0};

    //Sharedprefernce values

    public  final String MY_PREFS_NAME = "My_prefs_name";
    public  final String MY_PREFS_SETTINGS = "My_prefs_settings";

    public  String TREAT_NUMBER = "Treat_number";
    public  String  FIRST  = "First";

    public  int TREAT_NUM = 0;

    public  String VACUUM = "Vaccum";
    public  String PERCENT = "Percent";
    public  String X = "x";
    public  String PURGE_MEASURE = "purge_measure";
    public  String PURGE_HIGH = "purge_high";
    public  String PURGE_LOW = "purge_low";
    public String PISTION_MIN = "Piston_min";
    public String DELTA_Q = "Delta_q";

    public  String BYTE_H = "High";
    public  String BYTE_L = "Low";

    public  String MID_LOW = "Mid_low";
    public  String MID_HIGH = "Mid_high";
    public  String MID = "Mid";
    public  String HIGH_LOW = "High_low";
    public  String HIGH_HIGH = "High_high";
    public String HIGH = "High";
    public  String LOW_LOW = "Low_low";
    public  String LOW_HIGH = "Low_high";
    public String LOW = "Low";
    public  String PURGE = "Purge";
    public  String TREAT_TIME = "Treat_time";
    public  String TREAT_MENU_TIME = "Treat_menu_time";

    public String A_MAX_NUMBER = "A_max";
    public String B_MAX_NUMBER = "=B_max";
    public String A_B_MIN_NUMBER = "A_b_min";
    public String COUNTA_NUMBER = "Counta";
    public String GAS_RELEASE_NUMBER = "Gas_release_number";
    public String MIX_TIME_NUMBER = "Mix_time_number";
    public String INDEX_NUMBER = "Index_number";


    public String GAS_RELEASE_NUMBER_LOW = "Gas_release_number_low";
    public String GAS_RELEASE_NUMBER_HIGH = "Gas_release_number_high";
    public String MIX_TIME_NUMBER_LOW = "Mix_time_number_low";
    public String MIX_TIME_NUMBER_HIGH = "Mix_time_number_high";
    public String A_MAX_LOW = "A_max_low";
    public String A_MAX_HIGH = "A_max_high";
    public String B_MAX_LOW = "=B_max_low";
    public String B_MAX_HIGH = "=B_max_high";
    public String A_B_MIN_LOW = "A_b_min_low";
    public String A_B_MIN_HIGH = "A_b_min_high";
    public String COUNTA_LOW = "Counta_low";
    public String COUNTA_HIGH = "Counta_high";
    public String VACUUM_STATE = "Vacuum_state";



   //fragment tag
    public final String SPLASH_TAG = "Splash_tag";
    public final String MIX_TAG = "Mix_tag";
    public final String TREAT_SCREEN_TAG = "Treat_screen_tag";
    public final String TREAT_MENU_TAG = "Treat_menu_tag";
    public final String WELCOME = "Welcome_tag";
    public final String VACUUM_SCREEN_TAG = "Vacuum_screen_tag";
    public final String SETTINGS_TAG = "Settings_tag";
    public final String END_SESSION_TAG = "End_session_tag";
    public final String PURGE_TAG = "Purge_tag";
    public final String WELCOME_TAG = "Welcome_tag";

    public boolean PISTON_BOOLEAN;











    public Const(){

    }

    public void setVACUUM_BITHIGH(byte b){
        VACUUM_ON[4] = b;
        VACCUM_RELEASE[4] =b;
    }
    public byte getVACUUM_BITHIGH(){
        return VACUUM_ON[5];
    }
    public void setVACUUM_BITLOW(byte b){
        VACUUM_ON[5] = b;
        VACCUM_RELEASE[5] = b;

    }
    public void setVACUUM_OFF(byte high, byte low){
        VACUUM_OFF[4] = high;
        VACUUM_OFF[5] = low;
    }
    public byte getVACUUM_BITLOW(){
        return  VACUUM_ON[5];
    }

    public void setTREAT_NUM(){
        TREAT_NUM++;

    }
    public int getTREAT_NUM() {
        return TREAT_NUM;

    }

    public void setPURGE_ON(byte b_low,byte b_high){
        PURGE_ON[4] = b_high;
        PURGE_ON[5] = b_low;

    }
    public void setCOUNTA(byte b_low,byte b_high){
        COUNTA[4] = b_high;
        COUNTA[5] = b_low;

    }
    public void setA_MAX(byte b_low,byte b_high){
        A_MAX[4] = b_high;
        A_MAX[5] = b_low;

    }

    public void setB_MAX(byte b_low,byte b_high){
        B_MAX[4] = b_high;
        B_MAX[5] = b_low;
    }
    public void setA_B_MIN(byte b_low,byte b_high){
        A_B_MIN[4] = b_high;
        A_B_MIN[5] = b_low;
    }
    public void setGAS_RELEAS(byte b_low,byte b_high){
        GAS_RELEAS[4] = b_high;
        GAS_RELEAS[5] = b_low;
    }
    public void setMIX_TIME(byte b_low,byte b_high){
        MIX_TIME[4] = b_high;
        MIX_TIME[5] = b_low;
    }

    public void setMIXINGindex(byte index){
        MIX_TIME[0] = index;
    }

    public void setTREATMENTindex(byte index){
        TREATMENT[0] = index;
    }

    public void setVACUUM_ONindex(byte index){
        VACUUM_ON[0] = index;
    }

    public void setTREATMENT_COUNTINUEindex(byte index){
        TREATMENT_COUNTINUE[0] = index;
    }

    public void setPURGE_ONindex(byte index){
        PURGE_ON[0] = index;
    }
    public void setVACCUM_RELEASEindex(byte index){
        VACCUM_RELEASE[0] = index;
    }

    public void setEND_STOPindex(byte index){
        END_STOP[0] = index;
    }

    public void setPISTON_BOOLEAN(boolean bol){
        PISTON_BOOLEAN = bol;
    }

}
