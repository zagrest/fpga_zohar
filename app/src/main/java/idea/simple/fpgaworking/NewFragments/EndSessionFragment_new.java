package idea.simple.fpgaworking.NewFragments;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import idea.simple.fpgaworking.Constance.Const;
import idea.simple.fpgaworking.FPGACommunicator.FPGACommucnicator;
import idea.simple.fpgaworking.R;

/**
 * Created by avivvegh on 9/7/15.
 */
public class EndSessionFragment_new extends Fragment {

    FPGACommucnicator obj;
    View v;
    ImageView imEsNext;
    SharedPreferences.Editor editor;
    Const con;
    public EndSessionFragment_new(FPGACommucnicator obj){
        this.obj = obj;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.activity_end_session, container, false);
        con = new Const();
        editor = getActivity().getSharedPreferences(con.MY_PREFS_NAME, getActivity().MODE_PRIVATE).edit();
        editor.putInt(con.TREAT_NUMBER,0);
        editor.commit();
        imEsNext = (ImageView) v.findViewById(R.id.imEsNext);
        imEsNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return v;
    }
}
