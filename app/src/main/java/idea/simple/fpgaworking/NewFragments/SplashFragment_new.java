package idea.simple.fpgaworking.NewFragments;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import idea.simple.fpgaworking.Constance.Const;
import idea.simple.fpgaworking.FPGACommunicator.FPGACommucnicator_new;
import idea.simple.fpgaworking.R;

/**
 * Created by avivvegh on 9/6/15.
 */
public class SplashFragment_new extends android.app.Fragment {

    FPGACommucnicator_new communicator;

    SharedPreferences.Editor editor;
    Const con;
    public SplashFragment_new(FPGACommucnicator_new communicator){
        this.communicator = communicator;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_splash, container, false);
//        communicator = new FPGACommucnicator(getActivity());
        con = new Const();
        editor = getActivity().getSharedPreferences(con.MY_PREFS_NAME, getActivity().MODE_PRIVATE).edit();
        editor.putInt(con.TREAT_NUMBER,0);
        editor.putBoolean(con.FIRST, true);
        editor.putInt(con.TREAT_MENU_TIME, 5);
        editor.putInt(con.INDEX_NUMBER, 0);
        editor.putInt(con.VACUUM_STATE, 1);
        editor.putInt(con.PISTION_MIN,0);

        editor.commit();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);//zohar

                    // Do some stuff
                } catch (Exception e) {
                    e.getLocalizedMessage();
                } finally {
                    WelcomeFragment_new fragment2 = new WelcomeFragment_new(communicator);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frameLayout, fragment2,con.WELCOME_TAG);
//                    fragmentTransaction.addToBackStack(con.SPLASH_TAG);
                    fragmentTransaction.commit();
                }
            }
        }).start();
        return v;
    }
}
