package idea.simple.fpgaworking.NewFragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import idea.simple.fpgaworking.Constance.Const;
import idea.simple.fpgaworking.FPGACommunicator.FPGACommucnicator_new;
import idea.simple.fpgaworking.R;

/**
 * Created by avivvegh on 9/7/15.
 */
public class SettingsFragment_new extends Fragment implements View.OnClickListener {

    FPGACommucnicator_new obj;
    View v;
    Button bDone,bVaccumOk,bPercentOk,bXOk,bPurgeOK,bHighOK,bLowOk,bMidOK,bTreatOK,bAMAX,bBMAX,bABMin,bCounta,bTreatNumber,bGasRelease;
    EditText etSetVaccum,etSetPercent,etSetX,etHighset,etLowSet,etMidset,etPurgeSet,etTreatTimeout,etAMAX,etBMAX,etABMin,etCounta,etTreatNumber,etGasRelease;
    SharedPreferences.Editor editor;

    SharedPreferences prefs;
    Const con;

    public SettingsFragment_new(FPGACommucnicator_new obj){
        this.obj = obj;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.setting_fragment_layout, container, false);
        con = new Const();
        editor = getActivity().getSharedPreferences(con.MY_PREFS_NAME, getActivity().MODE_PRIVATE).edit();
        prefs = getActivity().getSharedPreferences(con.MY_PREFS_NAME, getActivity().MODE_PRIVATE);




        bDone = (Button) v.findViewById(R.id.bDoneSettings);
        bDone.setOnClickListener(this);
        bVaccumOk = (Button) v.findViewById(R.id.bVaccumOk);
        bVaccumOk.setOnClickListener(this);
        bPercentOk = (Button) v.findViewById(R.id.bPercentOk);
        bPercentOk.setOnClickListener(this);
        bXOk =(Button) v.findViewById(R.id.bXOk);
        bXOk.setOnClickListener(this);
        bPurgeOK = (Button) v.findViewById(R.id.bPurgeOk);
        bPurgeOK.setOnClickListener(this);
        bHighOK = (Button) v.findViewById(R.id.bHighSet);
        bHighOK.setOnClickListener(this);
        bLowOk = (Button) v.findViewById(R.id.bLowSet);
        bLowOk.setOnClickListener(this);
        bMidOK = (Button) v.findViewById(R.id.bMidSet);
        bMidOK.setOnClickListener(this);
        bTreatOK = (Button) v.findViewById(R.id.bTreatTIme);
        bTreatOK.setOnClickListener(this);
        bAMAX= (Button) v.findViewById(R.id.bAMax);
        bAMAX.setOnClickListener(this);
        bBMAX = (Button) v.findViewById(R.id.bBMax);
        bBMAX.setOnClickListener(this);
        bABMin = (Button) v.findViewById(R.id.bABMin);
        bABMin.setOnClickListener(this);
        bCounta = (Button) v.findViewById(R.id.bCounta);
        bCounta.setOnClickListener(this);
        bTreatNumber = (Button) v.findViewById(R.id.bTreatNumber);
        bTreatNumber.setOnClickListener(this);
        bGasRelease = (Button) v.findViewById(R.id.bGasRelease);
        bGasRelease.setOnClickListener(this);

        etSetPercent = (EditText) v.findViewById(R.id.etSetPercent);
        etSetPercent.setText("" + prefs.getInt(con.PERCENT, 30));

        etSetX = (EditText) v.findViewById(R.id.etSetX);
        etSetX.setText("" + prefs.getInt(con.X, 0));

        etSetVaccum = (EditText) v.findViewById(R.id.etSetVaccum);
        etSetVaccum.setText("" + prefs.getInt(con.VACUUM, 2942));

        etGasRelease = (EditText) v.findViewById(R.id.etGasRelease);
        etGasRelease.setText(""+  prefs.getInt(con.GAS_RELEASE_NUMBER, (int) (20/0.1364223)));

        etTreatNumber = (EditText) v.findViewById(R.id.etTreatNumber);
        etTreatNumber.setText(""+  prefs.getInt(con.MIX_TIME_NUMBER,  0));

        etAMAX = (EditText) v.findViewById(R.id.etAMax);
        etAMAX.setText(""+  prefs.getInt(con.A_MAX_NUMBER, (int) (30/0.1364223)));

        etBMAX = (EditText) v.findViewById(R.id.etBMax);
        etBMAX.setText(""+  prefs.getInt(con.B_MAX_NUMBER, (int) (30/0.1364223)));

        etABMin = (EditText) v.findViewById(R.id.etABMin);
        etABMin.setText(""+  prefs.getInt(con.A_B_MIN_NUMBER, (int) (8/0.1364223)));

        etCounta = (EditText) v.findViewById(R.id.etCounta);
        etCounta.setText(""+  prefs.getInt(con.COUNTA_NUMBER, (int) (1/0.1364223)));


        etLowSet = (EditText) v.findViewById(R.id.etLowSet);
        etLowSet.setText("" + prefs.getInt(con.LOW, 2987));

        etMidset = (EditText) v.findViewById(R.id.etMidSet);
        etMidset.setText("" + prefs.getInt(con.MID, 3121));

        etHighset = (EditText) v.findViewById(R.id.etHighSet);
        etHighset.setText("" + prefs.getInt(con.HIGH, 3054));

        etPurgeSet = (EditText) v.findViewById(R.id.etPurgeSet);
        etPurgeSet.setText("" + prefs.getInt(con.PURGE_MEASURE, 2762));

        etTreatTimeout = (EditText) v.findViewById(R.id.etTreatTime);
        etTreatTimeout.setText("" + prefs.getInt(con.TREAT_TIME, 30000));


    ;


        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bDoneSettings:
                getActivity().getFragmentManager().popBackStack();
                WelcomeFragment_new fragment2 = new WelcomeFragment_new(obj);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frameLayout, fragment2);
                fragmentTransaction.commit();
                break;
            case R.id.bVaccumOk:



                break;
            case R.id.bPercentOk:
                editor.putInt(con.PERCENT, Integer.valueOf(etSetPercent.getText().toString()));
                editor.commit();
                break;
            case R.id.bXOk:
                editor.putInt(con.X, Integer.valueOf(etSetX.getText().toString()));
                editor.commit();
                break;
            case R.id.bPurgeOk:
                int high = Integer.valueOf(etPurgeSet.getText().toString())/256;
                int low = Integer.valueOf(etPurgeSet.getText().toString())%256;
                con.setPURGE_ON((byte) low, (byte) high);
                editor.putInt(con.PURGE_HIGH, high);
                editor.putInt(con.PURGE_LOW, low);
                editor.putInt(con.PURGE_MEASURE, Integer.valueOf(etPurgeSet.getText().toString()));
                editor.commit();
                break;
            case R.id.bHighSet:
                 high = Integer.valueOf(etHighset.getText().toString())/256;
                 low = Integer.valueOf(etHighset.getText().toString())%256;
                con.setVACUUM_BITHIGH((byte) high);
                con.setVACUUM_BITLOW((byte) low);

                editor.putInt(con.HIGH_HIGH, high);
                editor.putInt(con.HIGH_LOW, low);

                editor.commit();
                break;
            case R.id.bMidSet:
                high = Integer.valueOf(etMidset.getText().toString()) / 256;
                low = Integer.valueOf(etMidset.getText().toString()) % 256;
                con.setVACUUM_BITHIGH((byte) high);
                con.setVACUUM_BITLOW((byte) low);

                editor.putInt(con.MID_HIGH, high);
                editor.putInt(con.MID_LOW, low);

                editor.commit();
                break;
            case R.id.bLowSet:
                high = Integer.valueOf(etLowSet.getText().toString()) / 256;
                low = Integer.valueOf(etLowSet.getText().toString()) % 256;
                con.setVACUUM_BITHIGH((byte) high);
                con.setVACUUM_BITLOW((byte) low);

                editor.putInt(con.LOW_HIGH, high);
                editor.putInt(con.LOW_LOW, low);

                editor.commit();
                break;
            case R.id.bTreatTIme:
                int time = Integer.parseInt(etTreatTimeout.getText().toString());
                editor.putInt(con.TREAT_TIME,time);
                editor.commit();

                break;
            case R.id.bAMax:
                int calc = (int) (Integer.valueOf(etAMAX.getText().toString())/ 0.1364223);
                high = calc/256;
                low = calc%256;
                editor.putInt(con.A_MAX_NUMBER, Integer.valueOf(etAMAX.getText().toString()));

                editor.putInt(con.A_MAX_LOW,low);
                editor.putInt(con.A_MAX_HIGH, high);
                editor.commit();

                break;
            case R.id.bBMax:


                calc = (int) (Integer.valueOf(etBMAX.getText().toString()) / 0.1364223);
                high = calc/256;
                low = calc%256;
                editor.putInt(con.B_MAX_NUMBER, Integer.valueOf(etBMAX.getText().toString()));
                editor.putInt(con.B_MAX_LOW,low);
                editor.putInt(con.B_MAX_HIGH, high);
                editor.commit();

                break;
            case R.id.bABMin:
                calc = (int) (Integer.valueOf(etABMin.getText().toString()) / 0.1364223);
                high = calc/256;
                low = calc%256;
                editor.putInt(con.A_B_MIN_NUMBER, Integer.valueOf(etABMin.getText().toString()));

                editor.putInt(con.A_B_MIN_LOW,low);
                editor.putInt(con.A_B_MIN_HIGH,high);
                editor.commit();

                break;
            case R.id.bCounta:
                calc = (int) (Integer.valueOf(etCounta.getText().toString()) / 0.1364223);
                high = calc/256;
                low = calc%256;
                editor.putInt(con.COUNTA_NUMBER, Integer.valueOf(etCounta.getText().toString()));

                editor.putInt(con.COUNTA_LOW,low);
                editor.putInt(con.COUNTA_HIGH,high);
                editor.commit();

                break;
            case R.id.bTreatNumber:
//                high = Integer.valueOf(etTreatNumber.getText().toString()) / 256;
                low = Integer.valueOf(etTreatNumber.getText().toString()) ;

                editor.putInt(con.MIX_TIME_NUMBER,Integer.valueOf(etTreatNumber.getText().toString()));
//                editor.putInt(con.MIX_TIME_NUMBER_HIGH, high);
                editor.commit();
                break;
            case R.id.bGasRelease:

                calc = (int) (Integer.valueOf(etGasRelease.getText().toString()) / 0.1364223);
                high = calc/256;
                low = calc%256;
//
                editor.putInt(con.GAS_RELEASE_NUMBER,Integer.valueOf(etGasRelease.getText().toString()));

                editor.putInt(con.GAS_RELEASE_NUMBER_LOW,low);
                editor.putInt(con.GAS_RELEASE_NUMBER_HIGH,high);
                editor.commit();
                break;

        }

    }
}
