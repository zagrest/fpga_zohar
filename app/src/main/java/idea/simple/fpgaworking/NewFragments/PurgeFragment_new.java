package idea.simple.fpgaworking.NewFragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import idea.simple.fpgaworking.Constance.Const;
import idea.simple.fpgaworking.FPGACommunicator.FPGACommucnicator_new;
import idea.simple.fpgaworking.R;

/**
 * Created by avivvegh on 9/6/15.
 */
public class PurgeFragment_new extends Fragment {

    String str;
    View v;
    private int progressStatus = 0;
    private int progress = 0;
    SeekBar pbSeek;

    private Handler handler = new Handler();
    TextView tvProgressText,tvPurgeWarning;
    Typeface fontRobo;
    FPGACommucnicator_new obj;
    SharedPreferences.Editor editor;
    SharedPreferences prefs;

    Const con;
    public PurgeFragment_new(FPGACommucnicator_new obj){
//        this.str = str;
        this.obj = obj;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.activity_purge, container, false);

        con = new Const();
        fontRobo = Typeface.createFromAsset(getActivity().getAssets(), "fonts/gothic.ttf");
        prefs = getActivity().getSharedPreferences(con.MY_PREFS_NAME, getActivity().MODE_PRIVATE);

        tvPurgeWarning = (TextView) v.findViewById(R.id.tvPurgeWarning);
        tvPurgeWarning.setTypeface(fontRobo);
        tvProgressText = (TextView) v.findViewById(R.id.tvProgressText);
        pbSeek = (SeekBar) v.findViewById(R.id.pbSeekbar);
        pbSeek.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

        // new LoadingTask(pbProgress,this).execute("");
//        MyCount count = new MyCount(30000,1000);
        //count.start();
        new Thread(new Runnable() {
            public void run() {
                while (progressStatus < 100) {
                    progressStatus += 1;
                    // Update the progress bar and display the
                    //current value in the text view
                    handler.post(new Runnable() {
                        public void run() {
                            tvProgressText.setText("PURGE " + progressStatus +"%");
                            pbSeek.setProgress(progressStatus);
                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        //Just to display the progress slowly
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }


                obj.writeOpSettings(con.VACCUM_RELEASE);


            }
        }).start();


        return v;
    }

    public void toTreatMenu(){
        getActivity().getFragmentManager().popBackStack();
        TreatMenuFragment_new fragment2 = new TreatMenuFragment_new(obj);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment2,con.TREAT_MENU_TAG);
        fragmentTransaction.addToBackStack(con.PURGE_TAG);
        fragmentTransaction.commit();
    }



}