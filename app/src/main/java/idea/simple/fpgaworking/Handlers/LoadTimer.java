package idea.simple.fpgaworking.Handlers;

import android.os.AsyncTask;
import android.widget.ProgressBar;

/**
 * Created by David Lev on 18-Aug-15.
 */
public class LoadTimer extends AsyncTask<String, Integer, Integer> {

    public interface LoadingTaskFinishedListener{
        void onTaskFinsihed();
    }

    private final ProgressBar progressBar;
    private final LoadingTaskFinishedListener finishedListener;
    private final long time;

    public LoadTimer(ProgressBar progressBar, LoadingTaskFinishedListener finishedListener, long time){
        this.progressBar = progressBar;
        this.finishedListener = finishedListener;
        this.time = time;
    }

    @Override
    protected Integer doInBackground(String... params) {
        if (resourcesDontAlreadyEcist()){
            downloadResources();
        }
        return 1234;
    }

    private boolean resourcesDontAlreadyEcist(){
        return true;
    }
    private void downloadResources(){
        int count = (int)60;
        for (int i =0; i< count;i++){
            int progress = (int) ((i/(float) count)*100);
            publishProgress(progress);

            try{
                Thread.sleep(1000);

            }catch (InterruptedException ignore){

            }
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        progressBar.setProgress(values[0]);
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
        finishedListener.onTaskFinsihed();
    }
}
