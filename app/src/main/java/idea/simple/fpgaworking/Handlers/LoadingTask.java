package idea.simple.fpgaworking.Handlers;

import android.os.AsyncTask;
import android.widget.ProgressBar;

/**
 * Created by David Lev on 18-Aug-15.
 */
public class LoadingTask extends AsyncTask<String, Integer, Integer> {

    public interface LoadingTaskFinishedList{
        void onTaskFinsih();
    }

    private final ProgressBar progressBar;
    private final LoadingTaskFinishedList finishedListener;

    public LoadingTask(ProgressBar progressBar, LoadingTaskFinishedList finishedListener){
        this.progressBar = progressBar;
        this.finishedListener = finishedListener;
    }

    @Override
    protected Integer doInBackground(String... params) {
        if (resourcesDontAlreadyEcist()){
            downloadResources();
        }
        return 1234;
    }

    private boolean resourcesDontAlreadyEcist(){
        return true;
    }
    private void downloadResources(){
        int count = 15;
        for (int i =0; i< count;i++){
            int progress = (int) ((i/(float) count)*100);
            publishProgress(progress);

            try{
                Thread.sleep(50);

            }catch (InterruptedException ignore){

            }
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        progressBar.setProgress(values[0]);
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
        finishedListener.onTaskFinsih();
    }
}
