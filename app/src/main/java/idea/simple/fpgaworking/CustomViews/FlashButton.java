package idea.simple.fpgaworking.CustomViews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;

import idea.simple.fpgaworking.R;


/**
 * Created by David Lev on 17-Aug-15.
 */
public class FlashButton extends ImageButton {

    private FlashEnum mState;
    private FlashListener mFlashListener;

    public FlashButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                int next = ((mState.ordinal() + 1) % FlashEnum.values().length);
                setState(FlashEnum.values()[next]);
                performFlashClick();
            }
        });
        //Sets initial state
        setState(FlashEnum.AUTOMATIC);
    }

    private void performFlashClick() {
        if (mFlashListener == null) return;
        switch (mState) {
            case AUTOMATIC:
                mFlashListener.onAutomatic();
                break;
            case ON:
                mFlashListener.onOn();
                break;
            case OFF:
                mFlashListener.onOff();
                break;
        }
    }

    private void createDrawableState() {
        switch (mState) {
            case AUTOMATIC:
                setImageResource(R.drawable.lowtoggle);
                break;
            case ON:
                setImageResource(R.drawable.midtoggle);
                break;
            case OFF:
                setImageResource(R.drawable.hightoggle);
                break;
        }
    }

    public FlashEnum getState() {
        return mState;
    }

    public void setState(FlashEnum state) {
        if (state == null) return;
        this.mState = state;
        createDrawableState();

    }

    public FlashListener getFlashListener() {
        return mFlashListener;
    }

    public void setFlashListener(FlashListener flashListener) {
        this.mFlashListener = flashListener;
    }

    public enum FlashEnum {
        AUTOMATIC, ON, OFF
    }

    public interface FlashListener {
        void onAutomatic();

        void onOn();

        void onOff();
    }

}