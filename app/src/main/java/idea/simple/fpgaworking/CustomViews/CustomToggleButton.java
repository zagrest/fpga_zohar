package idea.simple.fpgaworking.CustomViews;

import android.content.Context;
import android.widget.ToggleButton;

/**
 * Created by David Lev on 17-Aug-15.
 */
public class CustomToggleButton extends ToggleButton {
    public CustomToggleButton(Context context) {
        super(context);
    }
}
