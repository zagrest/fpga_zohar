package idea.simple.fpgaworking.CustomViews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

import idea.simple.fpgaworking.Listeners.OnTimeListener;

/**
 * Created by Dimon_GDA on 8/26/15.
 */
public class ProgressView extends View {


    final static int OUTER_COLOR = 0x4Dc3c2c3;
    final static int PROGRESS_COLOR = 0xFF0ACCEF;
    final static int BACKGROUND_COLOR = 0xFFFFFFFF;
    final static int TITLE_COLOR = 0xFF000000;
    final static int ARROW_COLOR = 0xFF000000;
    final static int PROGRESS_CIRCLE_BORDER_COLOR = 0x4DEFEFEF;
    final static int PROGRESS_CIRCLE_COLOR = 0x4DFFFFFF;
    final static int BACKGROUND_LINE_WITH = 2;
    float BACKGROUND_CIRCLE_WITH;
    float PROGRESS_WITH;
    float PROGRESS_INDICATOR_BACKGROUND_WITH;
    float PROGRESS_INDICATOR_INNER_WITH;
    float PROGRESS_INDICATOR_WITH;
    float ARROW_WITH;
    final static int PROGRESS_START_ANGLE = -90;
    final static int CIRCLE_START_ANGLE = 135;
    final static String TEXT_HOLDER = "99:99";
    final static String TIME_FORMATTER = "%02d:%02d";


    private String firstTitle = "";
    private String secondTitle = "";

    private Paint paint = null;
    private RectF mBounds = new RectF();
    private float max = 600;
    private float progress = 0;
    private float minTextSize = 2;
    private Rect textRect;
    private Typeface fontr;
    private Typeface fontb;
    OnTimeListener mListener;

    public void setFirstTitle(String firstTitle) {
        this.firstTitle = firstTitle;
    }

    public void setSecondTitle(String secondTitle) {
        this.secondTitle = secondTitle;
    }

    public ProgressView(Context context) {
        super(context);
        init(context);
    }

    public ProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public ProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }


    public float getMax() {
        return max;
    }

    public void setMax(float max) {
        this.max = max;
        invalidate();
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {

        if (progress > max) {
            progress = max;
        }
        this.progress = progress;
        invalidate();
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        canvas.setDrawFilter(new PaintFlagsDrawFilter(1, Paint.ANTI_ALIAS_FLAG));
        super.dispatchDraw(canvas);
    }


    private void init(Context context) {
        this.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        paint = new Paint();
        textRect = new Rect();



        fontb = Typeface.createFromAsset(context.getAssets(), "fonts/gothic_bold.ttf");
        fontr = Typeface.createFromAsset(context.getAssets(), "fonts/gothic.ttf");

    }


    protected Path getVertLine() {
        Path vertLine = new Path();
        vertLine.setLastPoint(mBounds.left, mBounds.centerY() - BACKGROUND_LINE_WITH);
        vertLine.lineTo(mBounds.right, mBounds.centerY() - BACKGROUND_LINE_WITH);
        vertLine.lineTo(mBounds.right, mBounds.centerY() + BACKGROUND_LINE_WITH);
        vertLine.lineTo(mBounds.left, mBounds.centerY() + BACKGROUND_LINE_WITH);
        vertLine.lineTo(mBounds.left, mBounds.centerY() - BACKGROUND_LINE_WITH);
        return vertLine;
    }

    protected Path getHorLine() {
        Path horLine = new Path();
        horLine.setLastPoint(mBounds.centerX() - BACKGROUND_LINE_WITH, mBounds.top);
        horLine.lineTo(mBounds.centerX() - BACKGROUND_LINE_WITH, mBounds.bottom);
        horLine.lineTo(mBounds.centerX() + BACKGROUND_LINE_WITH, mBounds.bottom);
        horLine.lineTo(mBounds.centerX() + BACKGROUND_LINE_WITH, mBounds.top);
        horLine.lineTo(mBounds.centerX() - BACKGROUND_LINE_WITH, mBounds.top);
        return horLine;
    }

    protected Path getBackgroundCircle() {
        Path largePath = new Path();
        largePath.addCircle(mBounds.centerX(), mBounds.centerY(), mBounds.width() / 2, Path.Direction.CW);
        return largePath;
    }

    protected RectF getProgressRect() {
        RectF rec = new RectF();


        rec.set(mBounds.left + BACKGROUND_CIRCLE_WITH, mBounds.top + BACKGROUND_CIRCLE_WITH, mBounds.right - BACKGROUND_CIRCLE_WITH, mBounds.bottom - BACKGROUND_CIRCLE_WITH);
        return rec;
    }


    private float calcProgressAngle(float progress) {

        return 360 * progress;

    }

    @Override
    protected void onDraw(Canvas c) {
        super.onDraw(c);

        float x;
        float y;
        float angle = calcProgressAngle(progress / max);
        float rad = mBounds.width() / 2 - (BACKGROUND_CIRCLE_WITH - BACKGROUND_LINE_WITH);

        c.save();
        c.clipPath(getBackgroundCircle());
        c.clipPath(getVertLine(), Region.Op.DIFFERENCE);
        c.clipPath(getHorLine(), Region.Op.DIFFERENCE);

        paint.setStyle(Paint.Style.FILL);
        paint.setColor(OUTER_COLOR);
        c.drawCircle(mBounds.centerX(), mBounds.centerY(), mBounds.width() / 2, paint);
        c.restore();
        paint.setColor(PROGRESS_COLOR);
        c.drawCircle(mBounds.centerX(), mBounds.centerY(), rad, paint);

        paint.setColor(BACKGROUND_COLOR);
        c.drawArc(getProgressRect(), PROGRESS_START_ANGLE, angle, true, paint);
        paint.setColor(PROGRESS_COLOR);
        c.drawCircle(mBounds.centerX(), mBounds.centerY(), mBounds.width() / 2 - PROGRESS_WITH - BACKGROUND_CIRCLE_WITH - 2 * BACKGROUND_LINE_WITH, paint);
        paint.setColor(BACKGROUND_COLOR);
        c.drawCircle(mBounds.centerX(), mBounds.centerY(), mBounds.width() / 2 - BACKGROUND_CIRCLE_WITH - 3 * BACKGROUND_LINE_WITH - PROGRESS_WITH, paint);

        float xAngle = (float) ((angle + PROGRESS_START_ANGLE) * (Math.PI / 180));

        float x1 = mBounds.centerX() + (float) ((rad - 2 * BACKGROUND_LINE_WITH - PROGRESS_WITH / 2) * Math.cos(xAngle));
        float y1 = mBounds.centerY() + (float) ((rad - 2 * BACKGROUND_LINE_WITH - PROGRESS_WITH / 2) * Math.sin(xAngle));


        paint.setColor(PROGRESS_CIRCLE_BORDER_COLOR);
        c.drawCircle(x1, y1, PROGRESS_INDICATOR_BACKGROUND_WITH + BACKGROUND_LINE_WITH, paint);

        paint.setColor(PROGRESS_CIRCLE_COLOR);
        c.drawCircle(x1, y1, PROGRESS_INDICATOR_BACKGROUND_WITH, paint);
        paint.setColor(PROGRESS_COLOR);
        c.drawCircle(x1, y1, PROGRESS_INDICATOR_BACKGROUND_WITH - PROGRESS_INDICATOR_INNER_WITH, paint);
        paint.setColor(BACKGROUND_COLOR);
        c.drawCircle(x1, y1, PROGRESS_INDICATOR_BACKGROUND_WITH - PROGRESS_INDICATOR_INNER_WITH - PROGRESS_INDICATOR_WITH, paint);

        c.save();

        c.rotate(CIRCLE_START_ANGLE + angle, x1, y1);
        paint.setColor(ARROW_COLOR);
        c.drawRect(x1 - ARROW_WITH, y1 - ARROW_WITH, x1 + ARROW_WITH, y1 + ARROW_WITH, paint);
        paint.setColor(BACKGROUND_COLOR);
        c.drawRect(x1 - ARROW_WITH / 2, y1 - ARROW_WITH / 2, x1 + ARROW_WITH, y1 + ARROW_WITH, paint);


        c.restore();

        paint.setColor(TITLE_COLOR);
        paint.setTextSize(minTextSize);

        paint.setTypeface(Typeface.create(fontb, Typeface.NORMAL));


        if (firstTitle != null && firstTitle.length() > 1) {
            paint.getTextBounds(firstTitle, 0, firstTitle.length(), textRect);

            while (textRect.width() < mBounds.width() / 2.7 ) {
                paint.setTextSize(paint.getTextSize() + 1);
                paint.getTextBounds(firstTitle, 0, firstTitle.length(), textRect);
            }

            x = mBounds.width() / 2f - textRect.width() / 2f;
            y = mBounds.height() / 4f + textRect.height() / 2f + BACKGROUND_CIRCLE_WITH;
            c.drawText(firstTitle, x, y, paint);
        }

        int leftMinutes = 0;
        int leftSeconds = 0;

        if (max >= progress) {
            leftMinutes = (int) ((max - progress) / 60);
            leftSeconds = (int) (max - leftMinutes * 60 - progress);
        }


        String left = String.format(TIME_FORMATTER, leftMinutes, leftSeconds);

        paint.setColor(PROGRESS_COLOR);
        paint.setTextSize(minTextSize);
        paint.setTypeface(Typeface.create(fontr, Typeface.NORMAL));
        paint.getTextBounds(TEXT_HOLDER, 0, TEXT_HOLDER.length(), textRect);

        while (textRect.width() < mBounds.width() * 0.5) {
            paint.setTextSize(paint.getTextSize() + 1);
            paint.getTextBounds(TEXT_HOLDER, 0, TEXT_HOLDER.length(), textRect);
        }

        x = mBounds.width() / 2f - textRect.width() / 2f;
        y = mBounds.height() / 2.25f + textRect.height() / 2f;
        c.drawText(left, x, y, paint);

        paint.setColor(TITLE_COLOR);
        c.drawLine(x, y + PROGRESS_WITH, x + textRect.width(), y + PROGRESS_WITH, paint);

        paint.setColor(TITLE_COLOR);
        paint.setTextSize(minTextSize);

        paint.setTypeface(Typeface.create(fontb, Typeface.BOLD));


        if (secondTitle != null && secondTitle.length() > 1) {
            paint.getTextBounds(secondTitle, 0, secondTitle.length(), textRect);

            while (textRect.width() < mBounds.width() / 2.5) {
                paint.setTextSize(paint.getTextSize() + 1);
                paint.getTextBounds(secondTitle, 0, secondTitle.length(), textRect);
            }

            x = mBounds.width() / 2f - textRect.width() / 2f;
            y = mBounds.height() / 2f + mBounds.height() / 8f + textRect.height() / 2f;
            c.drawText(secondTitle, x, y, paint);
        }

        int totalMinutes = (int) (progress / 60);
        int totalSeconds = (int) (progress - totalMinutes * 60);


        String total = String.format(TIME_FORMATTER, totalMinutes, totalSeconds);

        paint.setTextSize(minTextSize);
        paint.setTypeface(Typeface.create(fontr, Typeface.NORMAL));
        paint.getTextBounds(TEXT_HOLDER, 0, TEXT_HOLDER.length(), textRect);

        while (textRect.width() < mBounds.width() / 3) {
            paint.setTextSize(paint.getTextSize() + 1);
            paint.getTextBounds(TEXT_HOLDER, 0, TEXT_HOLDER.length(), textRect);
        }

        x = mBounds.width() / 2f - textRect.width() / 2f;
        y = mBounds.height() / 2f + mBounds.height() / 4f + textRect.height() / 2f;
        c.drawText(total, x, y, paint);

    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mBounds = new RectF(BACKGROUND_LINE_WITH*4, BACKGROUND_LINE_WITH*4, w - BACKGROUND_LINE_WITH*4, h - BACKGROUND_LINE_WITH*4);
        PROGRESS_WITH = mBounds.width() / 20;
        BACKGROUND_CIRCLE_WITH = mBounds.width() / 25;
        PROGRESS_INDICATOR_BACKGROUND_WITH = PROGRESS_WITH*1.7f;
        PROGRESS_INDICATOR_INNER_WITH = PROGRESS_INDICATOR_BACKGROUND_WITH /5f;
        PROGRESS_INDICATOR_WITH = PROGRESS_INDICATOR_BACKGROUND_WITH /5;
        ARROW_WITH = PROGRESS_INDICATOR_BACKGROUND_WITH /5;
    }

}
