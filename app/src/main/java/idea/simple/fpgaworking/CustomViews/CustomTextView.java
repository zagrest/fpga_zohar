package idea.simple.fpgaworking.CustomViews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by David Lev on 19-Aug-15.
 */
public class CustomTextView extends TextView {
    public CustomTextView(Context context) {
        super(context);
    }
    public CustomTextView(Context context,AttributeSet attrs) {
        super(context,attrs);
        init(attrs);
    }

    private void init(AttributeSet attrs) {

        if (attrs!=null) {

            if (  attrs.toString().equals("1")) {
                Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/gothic.ttf");
                setTypeface(myTypeface);
            }
            if (  attrs.toString().equals("2")) {
                Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/gothic_bold.ttf");
                setTypeface(myTypeface);
            }
        }
    }
}
