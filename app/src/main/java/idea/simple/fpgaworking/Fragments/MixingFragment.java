package idea.simple.fpgaworking.Fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import idea.simple.fpgaworking.Constance.Const;
import idea.simple.fpgaworking.FPGACommunicator.FPGACommucnicator;
import idea.simple.fpgaworking.Listeners.OnUSBEvenetListener;
import idea.simple.fpgaworking.R;

/**
 * Created by avivvegh on 9/6/15.
 */
public class MixingFragment extends Fragment implements View.OnClickListener {

    View v;
    ImageView imMixButton;
    FPGACommucnicator communicator;
    int counta_low,counta_high,a_max_low,a_max_high,b_max_high,b_max_low,a_b_min_high,a_b_min_low,gas_low,gas_high,mix_low,mix_high;


    SharedPreferences prefs;


    Const con;
    public MixingFragment(FPGACommucnicator obj){
        communicator = obj;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.activity_mixing, container, false);
        con = new Const();
        imMixButton = (ImageView) v.findViewById(R.id.im_mix_button);
        imMixButton.setOnClickListener(this);
        prefs = getActivity().getSharedPreferences(con.MY_PREFS_NAME, getActivity().MODE_PRIVATE);

        counta_low = prefs.getInt(con.COUNTA_LOW, 7);
        counta_high = prefs.getInt(con.COUNTA_HIGH, 0);
        con.setCOUNTA((byte)counta_low,(byte)counta_high);
//        con.setCOUNTA((byte)7,(byte)0);
        a_max_low =prefs.getInt(con.A_MAX_LOW,236);
        a_max_high =prefs.getInt(con.A_MAX_HIGH,3);
        con.setA_MAX((byte) a_max_low, (byte) a_max_high);
//        con.setA_MAX((byte) 236, (byte) 3);

        b_max_low =prefs.getInt(con.B_MAX_LOW,3);
        b_max_high =prefs.getInt(con.B_MAX_HIGH,236);
        con.setB_MAX((byte) b_max_low, (byte) b_max_high);
//        con.setB_MAX((byte) 236, (byte) 3);

        a_b_min_low =prefs.getInt(con.A_B_MIN_LOW,58);
        a_b_min_high =prefs.getInt(con.A_B_MIN_HIGH,0);
        con.setA_B_MIN((byte) a_b_min_low, (byte) a_b_min_high);
//        con.setA_B_MIN((byte) 58, (byte) 0);

        gas_low =prefs.getInt(con.GAS_RELEASE_NUMBER_LOW,0);
        gas_high =prefs.getInt(con.GAS_RELEASE_NUMBER_HIGH,0);
        con.setGAS_RELEAS((byte) gas_low, (byte) gas_high);
//        con.setGAS_RELEAS((byte) 0, (byte) 0);

        mix_low =prefs.getInt(con.MIX_TIME_NUMBER,12);
//        mix_high =prefs.getInt(con.MIX_TIME_NUMBER_HIGH,0);
        con.setMIX_TIME((byte)mix_low, (byte) 0);
//        con.setMIX_TIME((byte)12,(byte)0);
//        con.setMIX_TIME((byte)1,(byte)0);


        return v;
    }

    @Override
    public void onClick(View v) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                communicator.writeOpSettings(con.COUNTA);
                communicator.writeOpSettings(con.GAS_RELEAS);
                communicator.writeOpSettings(con.A_MAX);
                communicator.writeOpSettings(con.B_MAX);
                communicator.writeOpSettings(con.A_B_MIN);
                communicator.writeOpSettings(con.MIX_TIME);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                communicator.writeOpCode(con.MIXING ,new OnUSBEvenetListener() {
                    @Override
                    public void onAck() {

                        Toast.makeText(getActivity(), "Ack!!", Toast.LENGTH_LONG).show();
                        getActivity().getFragmentManager().popBackStack();
                        TreatMenuFragment fragment2 = new TreatMenuFragment(communicator);
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.frameLayout, fragment2);
                        fragmentTransaction.commit();

                    }

                    @Override
                    public void onDone() {

                    }

                    @Override
                    public void onError() {

                    }

                    @Override
                    public void onProccess() {

                    }
                });

            }
        }).start();

        //TODO: CHANGE BACK TO LISTNER


    }
}