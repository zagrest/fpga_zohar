package idea.simple.fpgaworking.Fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import idea.simple.fpgaworking.Constance.Const;
import idea.simple.fpgaworking.FPGACommunicator.FPGACommucnicator;
import idea.simple.fpgaworking.Listeners.OnUSBEvenetListener;
import idea.simple.fpgaworking.R;

/**
 * Created by avivvegh on 9/6/15.
 */
public class TreatMenuFragment extends Fragment implements View.OnClickListener {

    View v;
    ImageView imNext;
    TextView tvTreatNumber,tvMin5, tvMin6,tvMin7,tvMin8,tvMin9, tvMin10,tvNumApplicationDone,tvTODO,tv_title_num_application,tvMinAppText,bUsername;
    ImageView imMinChose5, imMinChose6,imMinChose7,imMinChose8,imMinChose9,imMinChose10,imStopTreat;
    RelativeLayout rlNum5,rlNum6,rlNum7,rlNum8,rlNum9,rlNum10;
    String tosend;
    Typeface fontRobo;
    private boolean check = false;
    Const con;
    FPGACommucnicator obj;
    int x_value;
    int treat_number;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    int state;
    int progress_state;
    int sum;
    public TreatMenuFragment(FPGACommucnicator obj){
        this.obj = obj;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.activity_treatment_menu, container, false);
        fontRobo = Typeface.createFromAsset(getActivity().getAssets(), "fonts/gothic_bold.ttf");

        con = new Const();
        prefs = getActivity().getSharedPreferences(con.MY_PREFS_NAME, getActivity().MODE_PRIVATE);
        editor = getActivity().getSharedPreferences(con.MY_PREFS_NAME, getActivity().MODE_PRIVATE).edit();

        x_value= prefs.getInt(con.X, 145);
        treat_number = prefs.getInt(con.TREAT_NUMBER,0);
//        if (x_value != 0) {
//            String name = prefs.getString("name", "No name defined");//"No name defined" is the default value.
//            int idName = prefs.getInt("idName", 0); //0 is the default value.
//        }
        init();
        return v;
    }

    private void init() {
        imStopTreat = (ImageView) v.findViewById(R.id.imUniqStop);
        imStopTreat.setOnClickListener(this);
        imNext = (ImageView) v.findViewById(R.id.imTreatNext);
        imNext.setOnClickListener(this);

        bUsername = (TextView) v.findViewById(R.id.bUsername);
        bUsername.setTypeface(fontRobo);

        tv_title_num_application = (TextView) v.findViewById(R.id.tv_title_num_application);
        tv_title_num_application.setTypeface(fontRobo);

        tvTreatNumber = (TextView) v.findViewById(R.id.tvTreatNumber);
        tvTreatNumber.setTypeface(fontRobo);
        tvTreatNumber.setText("" + treat_number);
        tvMinAppText = (TextView) v.findViewById(R.id.tvMinAppText);
        tvMinAppText.setTypeface(fontRobo);
        tvTODO = (TextView) v.findViewById(R.id.tvTodo);
        tvTODO.setTypeface(fontRobo);
        tvMin5 = (TextView) v.findViewById(R.id.tvMin5);
        tvMin5.setTypeface(fontRobo);
        tvMin6 =  (TextView) v.findViewById(R.id.tvMin6);
        tvMin6.setTypeface(fontRobo);

        tvMin7 = (TextView) v.findViewById(R.id.tvMin7);
        tvMin7.setTypeface(fontRobo);
        tvMin8 = (TextView) v.findViewById(R.id.tvMin8);
        tvMin8.setTypeface(fontRobo);

        tvMin9 = (TextView) v.findViewById(R.id.tvMin9);
        tvMin9.setTypeface(fontRobo);

        tvMin10 = (TextView) v.findViewById(R.id.tvMin10);
        tvMin10.setTypeface(fontRobo);

        tvNumApplicationDone = (TextView) v.findViewById(R.id.tvNumApplicationDone);
        tvNumApplicationDone.setTypeface(fontRobo);
        imMinChose5 = (ImageView) v.findViewById(R.id.imMinChose5);
        imMinChose6 = (ImageView) v.findViewById(R.id.imMinChose6);
        imMinChose7 = (ImageView) v.findViewById(R.id.imMinChose7);
        imMinChose8 = (ImageView) v.findViewById(R.id.imMinChose8);
        imMinChose9 = (ImageView) v.findViewById(R.id.imMinChose9);
        imMinChose10 = (ImageView) v.findViewById(R.id.imMinChose10);

        rlNum5 = (RelativeLayout) v.findViewById(R.id.rlNum5);
        rlNum6 = (RelativeLayout) v.findViewById(R.id.rlNum6);
        rlNum7 = (RelativeLayout) v.findViewById(R.id.rlNum7);
        rlNum8 = (RelativeLayout) v.findViewById(R.id.rlNum8);
        rlNum9 = (RelativeLayout) v.findViewById(R.id.rlNum9);
        rlNum10 = (RelativeLayout) v.findViewById(R.id.rlNum10);

        rlNum5.setOnClickListener(this);
        rlNum6.setOnClickListener(this);
        rlNum7.setOnClickListener(this);
        rlNum8.setOnClickListener(this);
        rlNum9.setOnClickListener(this);
        rlNum10.setOnClickListener(this);


        //todo:may be delete check state
//        check_state(check);

        int pos = prefs.getInt(con.TREAT_MENU_TIME,5);
        setTreatTime(pos);
    }

    private void setTreatTime(int positon) {
        switch (positon){
            case 5:
                funcNum(5);
                editor.putInt(con.TREAT_MENU_TIME, 5);
                editor.commit();
                break;
            case 6:
                funcNum(6);
                editor.putInt(con.TREAT_MENU_TIME, 6);
                editor.commit();
                break;
            case 7:
                funcNum(7);
                editor.putInt(con.TREAT_MENU_TIME, 7);
                editor.commit();
                break;
            case 8:
                funcNum(8);
                editor.putInt(con.TREAT_MENU_TIME, 8);
                editor.commit();
                break;
            case 9:
                funcNum(9);
                editor.putInt(con.TREAT_MENU_TIME, 9);
                editor.commit();
                break;
            case 10:
                funcNum(10);
                editor.putInt(con.TREAT_MENU_TIME, 10);
                editor.commit();
                break;
        }
    }


    private void check_state(boolean check){
        if(check == false){
            imMinChose5.setVisibility(View.VISIBLE);
            imMinChose6.setVisibility(View.INVISIBLE);
            imMinChose7.setVisibility(View.INVISIBLE);
            imMinChose8.setVisibility(View.INVISIBLE);
            imMinChose9.setVisibility(View.INVISIBLE);
            imMinChose10.setVisibility(View.INVISIBLE);
            tvMin5.setTextColor(Color.parseColor("#0accef"));
            tvMin6.setTextColor(Color.BLACK);
            tvMin7.setTextColor(Color.BLACK);
            tvMin8.setTextColor(Color.BLACK);
            tvMin9.setTextColor(Color.BLACK);
            tvMin10.setTextColor(Color.BLACK);
            this.check = true;
            tosend = "5";
            setCustomTextSize(5);
            funcNum(5);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imUniqStop:

                obj.writeOpCode(con.END_STOP, new OnUSBEvenetListener() {
                    @Override
                    public void onAck() {
                        getActivity().getFragmentManager().popBackStack();
                        WelcomeFragment fragment2 = new WelcomeFragment(obj);
                        final FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.frameLayout, fragment2);
                        fragmentTransaction.commit();
                    }

                    @Override
                    public void onDone() {

                    }

                    @Override
                    public void onError() {

                    }

                    @Override
                    public void onProccess() {

                    }
                });



                break;

            case R.id.imTreatNext:
                if (check == true) {

                    editor = getActivity().getSharedPreferences(con.MY_PREFS_NAME, getActivity().MODE_PRIVATE).edit();
                    int i = prefs.getInt(con.TREAT_NUMBER, 0);
                    i++;
                    editor.putInt(con.TREAT_NUMBER, i);
                    editor.commit();
                    getActivity().getFragmentManager().popBackStack();
                    TreatScreenFragment_new fragment3 = new TreatScreenFragment_new(tosend,obj,treat_number,progress_state,sum);

//                                    TreatScreenFragment fragment3 = new TreatScreenFragment(tosend,obj,state,progress_state);
                    FragmentManager fragmentManager2 = getFragmentManager();
                    FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
                    fragmentTransaction2.replace(R.id.frameLayout, fragment3);
                    fragmentTransaction2.commit();


                    /*
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            int low = prefs.getInt(con.HIGH_LOW,50);
                            int high = prefs.getInt(con.HIGH_HIGH,12);
                            con.setVACUUM_BITHIGH((byte) high);
                            con.setVACUUM_BITLOW((byte) low);

                            obj.writeOpSettings(con.VALVE);
                            obj.writeOpSettings(con.VACUUM_ON);

                            editor = getActivity().getSharedPreferences(con.MY_PREFS_NAME, getActivity().MODE_PRIVATE).edit();
                            int i = prefs.getInt(con.TREAT_NUMBER,0);
                            i++;
                            editor.putInt(con.TREAT_NUMBER,i );
                            editor.commit();

                            getActivity().getFragmentManager().popBackStack();
                            TreatScreenFragment_new fragment3 = new TreatScreenFragment_new(tosend,obj,treat_number,progress_state,sum);

//                                    TreatScreenFragment fragment3 = new TreatScreenFragment(tosend,obj,state,progress_state);
                            FragmentManager fragmentManager2 = getFragmentManager();
                            FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
                            fragmentTransaction2.replace(R.id.frameLayout, fragment3);
                            fragmentTransaction2.commit();

                            try {
                                Thread.sleep(50);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }


                        }
                    }).start();
                     */

//                    obj.writeOpSettings(Const.TREATMENT);
//                    getActivity().getFragmentManager().popBackStack();
//                    TreatScreenFragment fragment3 = new TreatScreenFragment(tosend,obj);
//                    FragmentManager fragmentManager2 = getFragmentManager();
//                    FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
//                    fragmentTransaction2.replace(R.id.frameLayout, fragment3);
//                    fragmentTransaction2.commit();




                } else
                    Toast.makeText(getActivity(), "Please chose minutes per application", Toast.LENGTH_LONG).show();


                break;
            case R.id.rlNum5:

                funcNum(5);
                setTreatTime(5);
                break;
            case R.id.rlNum6:

                funcNum(6);
                setTreatTime(6);
                break;
            case R.id.rlNum7:

                funcNum(7);
                setTreatTime(7);
                break;
            case R.id.rlNum8:

                funcNum(8);
                setTreatTime(8);
                break;
            case R.id.rlNum9:

                funcNum(9);
                setTreatTime(9);

                break;
            case R.id.rlNum10:

                funcNum(10);
                setTreatTime(10);
                break;



        }


    }

    private void setCustomTextSize(int num){
        switch (num){
            case 5:
                tvMin5.setTextSize(40);
                tvMin6.setTextSize(35);
                tvMin7.setTextSize(35);
                tvMin8.setTextSize(35);
                tvMin9.setTextSize(35);
                tvMin10.setTextSize(35);


                break;
            case 6:
                tvMin5.setTextSize(35);
                tvMin6.setTextSize(40);
                tvMin7.setTextSize(35);
                tvMin8.setTextSize(35);
                tvMin9.setTextSize(35);
                tvMin10.setTextSize(35);
                break;
            case 7:
                tvMin5.setTextSize(35);
                tvMin6.setTextSize(35);
                tvMin7.setTextSize(40);
                tvMin8.setTextSize(35);
                tvMin9.setTextSize(35);
                tvMin10.setTextSize(35);
                break;
            case 8:
                tvMin5.setTextSize(35);
                tvMin6.setTextSize(35);
                tvMin7.setTextSize(35);
                tvMin8.setTextSize(40);
                tvMin9.setTextSize(35);
                tvMin10.setTextSize(35);
                break;
            case 9:
                tvMin5.setTextSize(35);
                tvMin6.setTextSize(35);
                tvMin7.setTextSize(35);
                tvMin8.setTextSize(35);
                tvMin9.setTextSize(40);
                tvMin10.setTextSize(35);
                break;
            case 10:
                tvMin5.setTextSize(35);
                tvMin6.setTextSize(35);
                tvMin7.setTextSize(35);
                tvMin8.setTextSize(35);
                tvMin9.setTextSize(35);
                tvMin10.setTextSize(40);
                break;
        }

    }

    private void funcNum(int min){
        sum = (int) (x_value/1.6/min);
        switch (min){
            case 5:
                imMinChose5.setVisibility(View.VISIBLE);
                imMinChose6.setVisibility(View.INVISIBLE);
                imMinChose7.setVisibility(View.INVISIBLE);
                imMinChose8.setVisibility(View.INVISIBLE);
                imMinChose9.setVisibility(View.INVISIBLE);
                imMinChose10.setVisibility(View.INVISIBLE);
                tvMin5.setTextColor(Color.parseColor("#0accef"));
                tvMin6.setTextColor(Color.BLACK);
                tvMin7.setTextColor(Color.BLACK);
                tvMin8.setTextColor(Color.BLACK);
                tvMin9.setTextColor(Color.BLACK);
                tvMin10.setTextColor(Color.BLACK);
                check = true;
                tosend = "5";
                setCustomTextSize(5);

                state = 5;
                progress_state = 5;
                tvNumApplicationDone.setText(""+sum);
                break;
            case 6:
                imMinChose5.setVisibility(View.INVISIBLE);
                imMinChose6.setVisibility(View.VISIBLE);
                imMinChose7.setVisibility(View.INVISIBLE);
                imMinChose8.setVisibility(View.INVISIBLE);
                imMinChose9.setVisibility(View.INVISIBLE);
                imMinChose10.setVisibility(View.INVISIBLE);
                setCustomTextSize(6);
                state = 6;
                progress_state =6;

                tosend = "6";
                tvMin6.setTextColor(Color.parseColor("#0accef"));
                tvMin5.setTextColor(Color.BLACK);
                tvMin7.setTextColor(Color.BLACK);
                tvMin8.setTextColor(Color.BLACK);
                tvMin9.setTextColor(Color.BLACK);
                tvMin10.setTextColor(Color.BLACK);
                check = true;

                tvNumApplicationDone.setText(""+sum);
                break;
            case 7:
                imMinChose5.setVisibility(View.INVISIBLE);
                imMinChose6.setVisibility(View.INVISIBLE);
                imMinChose7.setVisibility(View.VISIBLE);
                imMinChose8.setVisibility(View.INVISIBLE);
                imMinChose9.setVisibility(View.INVISIBLE);
                imMinChose10.setVisibility(View.INVISIBLE);
                setCustomTextSize(7);


                tosend = "7";
                tvMin7.setTextColor(Color.parseColor("#0accef"));
                tvMin6.setTextColor(Color.BLACK);
                tvMin5.setTextColor(Color.BLACK);
                tvMin8.setTextColor(Color.BLACK);
                tvMin9.setTextColor(Color.BLACK);
                tvMin10.setTextColor(Color.BLACK);
                check = true;

                state = 7;
                progress_state =7;
                tvNumApplicationDone.setText(""+sum);
                break;
            case 8:
                imMinChose5.setVisibility(View.INVISIBLE);
                imMinChose6.setVisibility(View.INVISIBLE);
                imMinChose7.setVisibility(View.INVISIBLE);
                imMinChose8.setVisibility(View.VISIBLE);
                imMinChose9.setVisibility(View.INVISIBLE);
                imMinChose10.setVisibility(View.INVISIBLE);
                tosend = "8";
                tvMin8.setTextColor(Color.parseColor("#0accef"));
                tvMin6.setTextColor(Color.BLACK);
                tvMin7.setTextColor(Color.BLACK);
                tvMin5.setTextColor(Color.BLACK);
                tvMin9.setTextColor(Color.BLACK);
                tvMin10.setTextColor(Color.BLACK);
                setCustomTextSize(8);
                state = 8;
                progress_state =8;

                check = true;
                tvNumApplicationDone.setText(""+sum);
                break;
            case 9:
                imMinChose5.setVisibility(View.INVISIBLE);
                imMinChose6.setVisibility(View.INVISIBLE);
                imMinChose7.setVisibility(View.INVISIBLE);
                imMinChose8.setVisibility(View.INVISIBLE);
                imMinChose9.setVisibility(View.VISIBLE);
                imMinChose10.setVisibility(View.INVISIBLE);
                tosend = "9";
                tvMin9.setTextColor(Color.parseColor("#0accef"));
                setCustomTextSize(9);
                state = 9;
                progress_state =9;
                tvMin6.setTextColor(Color.BLACK);
                tvMin7.setTextColor(Color.BLACK);
                tvMin8.setTextColor(Color.BLACK);
                tvMin5.setTextColor(Color.BLACK);
                tvMin10.setTextColor(Color.BLACK);
                check = true;
                tvNumApplicationDone.setText(""+sum);
                break;
            case 10:
                imMinChose5.setVisibility(View.INVISIBLE);
                imMinChose6.setVisibility(View.INVISIBLE);
                imMinChose7.setVisibility(View.INVISIBLE);
                imMinChose8.setVisibility(View.INVISIBLE);
                imMinChose9.setVisibility(View.INVISIBLE);
                imMinChose10.setVisibility(View.VISIBLE);
                tosend = "10";
                tvMin10.setTextColor(Color.parseColor("#0accef"));
                tvMin6.setTextColor(Color.BLACK);
                tvMin7.setTextColor(Color.BLACK);
                tvMin8.setTextColor(Color.BLACK);
                tvMin9.setTextColor(Color.BLACK);
                tvMin5.setTextColor(Color.BLACK);
                setCustomTextSize(10);
                state = 10;
                progress_state =10;
                check = true;
                tvNumApplicationDone.setText(""+sum);
                break;
        }
    }
}