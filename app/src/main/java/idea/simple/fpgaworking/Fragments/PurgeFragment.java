package idea.simple.fpgaworking.Fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import idea.simple.fpgaworking.Constance.Const;
import idea.simple.fpgaworking.FPGACommunicator.FPGACommucnicator;
import idea.simple.fpgaworking.Listeners.OnUSBEvenetListener;
import idea.simple.fpgaworking.R;

/**
 * Created by avivvegh on 9/6/15.
 */
public class PurgeFragment extends Fragment {

    String str;
    View v;
    private int progressStatus = 0;
    private int progress = 0;
    SeekBar pbSeek;

    private Handler handler = new Handler();
    TextView tvProgressText,tvPurgeWarning;
    Typeface fontRobo;
    FPGACommucnicator obj;
    SharedPreferences.Editor editor;
    SharedPreferences prefs;

    Const con;
    public PurgeFragment(FPGACommucnicator obj){
//        this.str = str;
        this.obj = obj;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.activity_purge, container, false);

        con = new Const();
        fontRobo = Typeface.createFromAsset(getActivity().getAssets(), "fonts/gothic.ttf");
        prefs = getActivity().getSharedPreferences(con.MY_PREFS_NAME, getActivity().MODE_PRIVATE);

        tvPurgeWarning = (TextView) v.findViewById(R.id.tvPurgeWarning);
        tvPurgeWarning.setTypeface(fontRobo);
        tvProgressText = (TextView) v.findViewById(R.id.tvProgressText);
        pbSeek = (SeekBar) v.findViewById(R.id.pbSeekbar);
        pbSeek.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

        // new LoadingTask(pbProgress,this).execute("");
//        MyCount count = new MyCount(30000,1000);
        //count.start();
        new Thread(new Runnable() {
            public void run() {
                while (progressStatus < 100) {
                    progressStatus += 1;
                    // Update the progress bar and display the
                    //current value in the text view
                    handler.post(new Runnable() {
                        public void run() {
                            tvProgressText.setText("PURGE " + progressStatus +"%");
                            pbSeek.setProgress(progressStatus);
                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        //Just to display the progress slowly
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }


                obj.writeOpCode(con.VACCUM_RELEASE, new OnUSBEvenetListener() {
                    @Override
                    public void onAck() {
                        getActivity().getFragmentManager().popBackStack();
                        TreatMenuFragment fragment2 = new TreatMenuFragment(obj);
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.frameLayout, fragment2);
                        fragmentTransaction.commit();
                    }

                    @Override
                    public void onDone() {

                    }

                    @Override
                    public void onError() {

                    }

                    @Override
                    public void onProccess() {

                    }
                });
//                obj.writeOpCode(Const.PURGE_OFF, new OnUSBEvenetListener() {
//                    @Override
//                    public void onAck() {
//                        getActivity().getFragmentManager().popBackStack();
//                        TreatMenuFragment fragment2 = new TreatMenuFragment(obj);
//                        FragmentManager fragmentManager = getFragmentManager();
//                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                        fragmentTransaction.replace(R.id.frameLayout, fragment2);
//                        fragmentTransaction.commit();
//                    }
//
//                    @Override
//                    public void onDone() {
//
//                    }
//
//                    @Override
//                    public void onError() {
//
//                    }
//
//                    @Override
//                    public void onProccess() {
//
//                    }
//                });


            }
        }).start();
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    Thread.sleep(3000);
//
//                    // Do some stuff
//                } catch (Exception e) {
//                    e.getLocalizedMessage();
//                } finally {
//                    Intent toMain = new Intent(PurgeActivity.this, EndTreatmentActivity.class);
//                    startActivity(toMain);
//                    finish();
//                }
//            }
//        }).start();

        return v;
    }
    /*
    public class MyCount extends CountDownTimer {
        public MyCount(long millisFuture, long countdownInterval) {
            super(millisFuture, countdownInterval);

        }

        @Override
        public void onTick(long millisUntilFinished) {
            long millis3 = millisUntilFinished;
            progressStatus += 1;
            progress +=3;
            pbSeek.setProgress(progress);
            tvProgressText.setText("PURGE " + progressStatus + "%");

        }

        @Override
        public void onFinish() {
//            tvTimeStated.setText("11:00");
            Intent toMain = new Intent(PurgeActivity.this, TreatmentMenuActivity.class);
            startActivity(toMain);
        }
    }

    */



}