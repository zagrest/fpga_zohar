package idea.simple.fpgaworking.Fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import idea.simple.fpgaworking.Constance.Const;
import idea.simple.fpgaworking.CustomViews.ProgressView;
import idea.simple.fpgaworking.FPGACommunicator.FPGACommucnicator;
import idea.simple.fpgaworking.Handlers.LoadTimer;
import idea.simple.fpgaworking.Listeners.OnUSBEvenetListener;
import idea.simple.fpgaworking.R;

/**
 * Created by avivvegh on 9/9/15.
 */
public class TreatScreenFragment_new extends Fragment implements View.OnClickListener {


    String bundle;
    View v;
    ImageView imTreatStop;
    int state;
    private ProgressView mProgress;
    TextView tvTimeLeft,tvTimeStated,bUsername,tvTimeToFinish,tvTextTimeStart,tvHigh,tvMid,tvLow, tvTemp,tvVacuum;
    TextView im30percent ,tvConcTitle,tvCurrentTitle,tvTotalTitle,tvCurrentApp,tvTotalApps,tvPiston;
    Button bVacuumPlus,bVacuumMinus,bTempPlus,bTempMinus;
    TextView bVacuumControl,bTempControl;
    ImageView imTreatNum;
    LoadTimer loader;
    Typeface fontRobo;
    Timer timerStop;
    String time;
    Timer timer;
    FPGACommucnicator obj;
    Thread treat;
    int percent_value;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    Const con;
    Runnable r;
    Handler handler;
    Timer treatTimer;
    Timer tempIdicatiorController;

    int low;
    int low_low, low_high, high_low,high_high;
    int init_low = 2987,init_hihg= 3121;
    int init_reg_vcuum = 3054;
    int mid;
    int high;
    long treat_puls_time;
    int time_progress;
    int progress_state;
    //    long timerStop;
    long time_stop;
    int total;
    int index;
    int vacuum_state;
    boolean vacuum_on_off;
    int postion_max;
    int positon_min;
    int piston_percentage;
    public TreatScreenFragment_new(String bundle,FPGACommucnicator obj,int state,int progress_state,int total){
        this.bundle = bundle;
        this.obj = obj;
        this.state = state;
        this.progress_state =progress_state;
        this.total = total;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.treat_screen_fragment_layout, container, false);
        fontRobo = Typeface.createFromAsset(getActivity().getAssets(), "fonts/gothic.ttf");
        con = new Const();
        prefs = getActivity().getSharedPreferences(con.MY_PREFS_NAME, getActivity().MODE_PRIVATE);
        editor = getActivity().getSharedPreferences(con.MY_PREFS_NAME, getActivity().MODE_PRIVATE).edit();
        postion_max = prefs.getInt(con.A_MAX_NUMBER, 140);
        positon_min = prefs.getInt(con.PISTION_MIN,0);
        index = prefs.getInt(con.INDEX_NUMBER,0);
        percent_value= prefs.getInt(con.PERCENT, 30);
        treat_puls_time = prefs.getInt(con.TREAT_TIME,30000);

        if (bundle != null) {
            time = bundle;

        }
        setProgress(progress_state);
        state = prefs.getInt(con.TREAT_NUMBER,0);
//        setProgress(1);

        init();
        setCurrent_and_total(state, total);


        return v;
    }

    private void setCurrent_and_total(int state, int total) {
        tvCurrentApp.setText(""+state);
        tvTotalApps.setText(""+total);
    }

    public void setProgress(int times){

        switch (times){
            case 5:
                time_progress = times *60;
//                timerStop = times * 60000;
                time_stop = times * 59500;
                break;
            case 6:
                time_progress = times *60;
                time_stop = times * 59500;

                break;
            case 7:
                time_progress = times *60;
                time_stop = times * 59500;

                break;
            case 8:
                time_progress = times *60;
                time_stop = times * 59500;

                break;
            case 9:
                time_progress = times *60;
                time_stop = times * 59500;

                break;
            case 10:
                time_progress = times *60;
                time_stop = times * 59500;

                break;
            case 1:
                time_progress = times *60;
                time_stop = times * 59500;
                break;
        }

    }




    private void init() {
        vacuum_on_off = false;
        tvPiston = (TextView) v.findViewById(R.id.tvPiston);
        tvPiston.setTypeface(fontRobo);
        tvCurrentApp = (TextView) v.findViewById(R.id.tvCurrentApp);
        tvCurrentApp.setTypeface(fontRobo);
        tvTotalApps = (TextView) v.findViewById(R.id.tvTotalApps);
        tvTotalApps.setTypeface(fontRobo);

        tvConcTitle = (TextView) v.findViewById(R.id.tvConcTitle);
        tvConcTitle.setTypeface(fontRobo);
        tvCurrentTitle = (TextView) v.findViewById(R.id.tvCurrentTitle);
        tvCurrentTitle.setTypeface(fontRobo);
        tvTotalTitle = (TextView) v.findViewById(R.id.tvTotalTitle);
        tvTotalTitle.setTypeface(fontRobo);

        bVacuumControl = (TextView) v.findViewById(R.id.bVcuumControl);
        bVacuumControl.setOnClickListener(this);
        bTempControl = (TextView) v.findViewById(R.id.bTempControl);
        bTempControl.setOnClickListener(this);
        bVacuumPlus = (Button) v.findViewById(R.id.bSwitch1);
        bVacuumPlus.setOnClickListener(this);
        bVacuumMinus = (Button) v.findViewById(R.id.bSwitch3);
        bVacuumMinus.setOnClickListener(this);
        bTempPlus = (Button) v.findViewById(R.id.bSwitch4);
        bTempPlus.setOnClickListener(this);
        bTempMinus = (Button) v.findViewById(R.id.bSwitch6);
        bTempMinus.setOnClickListener(this);

        tvTemp = (TextView) v.findViewById(R.id.tvTemp);
        tvTemp.setTypeface(fontRobo);
        tvVacuum = (TextView) v.findViewById(R.id.tvVacuum);
        tvVacuum.setTypeface(fontRobo);

        im30percent = (TextView) v.findViewById(R.id.im30percent);
        im30percent.setTypeface(fontRobo);
        im30percent.setText(percent_value + "%");



        bUsername = (TextView) v.findViewById(R.id.bUsername);
        bUsername.setTypeface(fontRobo);
        imTreatStop = (ImageView) v.findViewById(R.id.imTreatStop);
        imTreatStop.setOnClickListener(this);
        mProgress = (ProgressView) v.findViewById(R.id.circle_progress_bar);
        mProgress.setFirstTitle("TIME TO FINISH:");
        mProgress.setSecondTitle("TIME FROM START:");
        mProgress.setMax(time_progress);//Total timer duration
        mProgress.setProgress(0);//Starting progress

        mProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //disable after first click
                mProgress.setClickable(false);

                treatTimer = new Timer();
                treatTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                obj.writeOpSettings(con.TREATMENT_COUNTINUE);


//                                obj.writePistionOpCode(con.PISTON_CURRENT_PLACE, new OnPistonLIster() {
//                                    @Override
//                                    public void onPistionPostion(byte high, byte low) {
//                                        double num = 0.0;
//                                        num = high * 256;
//                                        num += low;
//                                        num = num * 0.1364223;
//                                        int number = (int) num;
//                                        piston_percentage = number / (postion_max - positon_min);
//                                        tvPiston.setText(String.valueOf(piston_percentage));
//
//                                    }
//                                });

                            }
                        });
                    }
                }, 0, treat_puls_time);

//                mProgress.setEnabled(false);
//                obj.writeOpSettings(con.TREATMENT);
//                obj.writeOpCode(con.TREATMENT, new OnUSBEvenetListener() {
//                    @Override
//                    public void onAck() {
//                        Toast.makeText(getActivity(), "got callback", Toast.LENGTH_LONG).show();
//                    }
//
//                    @Override
//                    public void onDone() {
//                        Toast.makeText(getActivity(), "got callback", Toast.LENGTH_LONG).show();
//
//                    }
//
//                    @Override
//                    public void onError() {
//
//                    }
//
//                    @Override
//                    public void onProccess() {
//
//                    }
//                });

                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mProgress.setProgress(mProgress.getProgress() + 1f);
                            }
                        });
                    }
                }, 0, 1000);

                timerStop = new Timer();
                timerStop.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
//
//                        index++;
//                        editor.putInt(con.INDEX_NUMBER,index).commit();
//                        con.setPURGE_ONindex((byte) index);
                                if (tempIdicatiorController != null) {
                                    tempIdicatiorController.cancel();
                                }

                                try {
                                    Thread.sleep(100);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                obj.writeOpCode(con.PURGE_ON, new OnUSBEvenetListener() {
                                    @Override
                                    public void onAck() {
                                        getActivity().getFragmentManager().popBackStack();

                                        PurgeFragment fragment2 = new PurgeFragment(obj);
                                        FragmentManager fragmentManager = getFragmentManager();
                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                        fragmentTransaction.replace(R.id.frameLayout, fragment2);
                                        fragmentTransaction.commit();
                                        timer.cancel();
                                        timerStop.cancel();
                                        treatTimer.cancel();
                                        //todo: remove the comment after gal gives the functions
//                                        tempIdicatiorController.cancel();

                                    }

                                    @Override
                                    public void onDone() {

                                    }

                                    @Override
                                    public void onError() {

                                    }

                                    @Override
                                    public void onProccess() {

                                    }
                                });

                            }
                        });
                    }
                }, time_stop, time_stop);


                // todo: remove the comment when gal will do the functions


            }
        });

        tempIdicatiorController = new Timer();
        tempIdicatiorController.schedule(new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //*************************
                        //*************************
                        // temp indicator

                        obj.writeOpCode(con.TEMP_INDICATOR, new OnUSBEvenetListener() {//con.TEMP_INDICATOR
                            @Override
                            public void onAck() {

                            }

                            @Override
                            public void onDone() {

                            }

                            @Override
                            public void onError() {
                                bTempControl.setBackgroundResource(R.drawable.tempnotstable);

                            }

                            @Override
                            public void onProccess() {
                                bTempControl.setBackgroundResource(R.drawable.main_button);

                            }
                        });
                    }
                });
                //todo might change back 150 to 500
            }
        }, 0, 15);//zohar 150


//        if (prefs.getInt(con.TREAT_NUMBER,0) == 1) {


//        }


        vacuum_state = prefs.getInt(con.VACUUM_STATE,0);

//        vacuum_state(vacuum_state);

        //**************************
        //**************************
        //** write VACUUM_ON
        /*
        low = prefs.getInt(con.MID_LOW,238);
        high = prefs.getInt(con.MID_HIGH,11);
        con.setVACUUM_BITHIGH((byte) high);
        con.setVACUUM_BITLOW((byte) low);
        obj.writeOpSettings(con.VACUUM_ON);
        */
        //**************************
        //**************************






    }

    private void vacuum_state(int vacuum_state) {
        switch(vacuum_state){
            case 0:


//                low = prefs.getInt(con.LOW_LOW,140);
//                high = prefs.getInt(con.LOW_HIGH,12);


                //high !
                init_reg_vcuum +=67;
                high_high = init_reg_vcuum/256;
                high_low = init_reg_vcuum%256;
                con.setVACUUM_BITHIGH((byte) high_high);
                con.setVACUUM_BITLOW((byte) high_low);
                obj.writeOpSettings(con.VACUUM_ON);


                break;
            case 1:
//                low = prefs.getInt(con.MID_LOW,238);
//                high = prefs.getInt(con.MID_HIGH,11);

                //mid !
                /*
                low = prefs.getInt(con.HIGH_LOW,50);
                high = prefs.getInt(con.HIGH_HIGH,12);
                con.setVACUUM_BITHIGH((byte) high);
                con.setVACUUM_BITLOW((byte) low);
                obj.writeOpSettings(con.VACUUM_ON);
                editor.putInt(con.VACUUM_STATE, 2).commit();

                */
                if (vacuum_on_off == false) {
                    int low = prefs.getInt(con.HIGH_LOW, 50);
                    int high = prefs.getInt(con.HIGH_HIGH, 12);
                    con.setVACUUM_BITHIGH((byte) high);
                    con.setVACUUM_BITLOW((byte) low);

                    obj.writeOpSettings(con.VALVE);
                    obj.writeOpSettings(con.VACUUM_ON);


                    vacuum_on_off = true;
                }else{
                    con.setVACUUM_OFF((byte) high,(byte)low);
                    obj.writeOpSettings(con.VACUUM_OFF);
                    vacuum_on_off = false;
                }

                break;
            case 2:

                //low !
                init_reg_vcuum -=67;
                low_high = init_reg_vcuum/256;
                low_low = init_reg_vcuum%256;
                con.setVACUUM_BITHIGH((byte) low_high);
                con.setVACUUM_BITLOW((byte) low_low);
                obj.writeOpSettings(con.VACUUM_ON);
                editor.putInt(con.VACUUM_STATE, 0).commit();

                break;
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.bSwitch1:


                vacuum_state(2);
                break;

            case R.id.bSwitch3:


                vacuum_state(0);

                break;
            case R.id.bSwitch4:

                break;

            case R.id.bSwitch6:

                break;
            case R.id.bVcuumControl:


                vacuum_state(1);

                break;
            case R.id.bTempControl:
                break;

            case R.id.imTreatStop:

//                index++;
//                editor.putInt(con.INDEX_NUMBER,index).commit();
//                con.setPURGE_ONindex((byte) index);

                obj.writeOpSettings(con.PURGE_ON);
                getActivity().getFragmentManager().popBackStack();

                PurgeFragment fragment2 = new PurgeFragment(obj);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frameLayout, fragment2);
                fragmentTransaction.commit();
                if (timer !=null && timerStop !=null && treatTimer !=null){
                    timer.cancel();
                    timerStop.cancel();
                    treatTimer.cancel();
                }


                if (tempIdicatiorController !=null){
                    tempIdicatiorController.cancel();
                }

                /*
                obj.writeOpCode(con.PURGE_ON, new OnUSBEvenetListener() {
                    @Override
                    public void onAck() {
                        getActivity().getFragmentManager().popBackStack();

                        PurgeFragment fragment2 = new PurgeFragment(obj);
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.frameLayout, fragment2);
                        fragmentTransaction.commit();
                        if (timer !=null && timerStop !=null && treatTimer !=null){
                            timer.cancel();
                            timerStop.cancel();
                            treatTimer.cancel();
                        }

                        if (tempIdicatiorController !=null){
                            tempIdicatiorController.cancel();
                        }


                    }

                    @Override
                    public void onDone() {

                    }

                    @Override
                    public void onError() {

                    }

                    @Override
                    public void onProccess() {

                    }
                });


                */
                break;
        }

    }





}
