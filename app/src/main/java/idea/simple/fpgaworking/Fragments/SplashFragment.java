package idea.simple.fpgaworking.Fragments;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import idea.simple.fpgaworking.Constance.Const;
import idea.simple.fpgaworking.FPGACommunicator.FPGACommucnicator;
import idea.simple.fpgaworking.R;

/**
 * Created by avivvegh on 9/6/15.
 */
public class SplashFragment extends android.app.Fragment {

    FPGACommucnicator communicator;

    SharedPreferences.Editor editor;
    Const con;
    public SplashFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_splash, container, false);
        communicator = new FPGACommucnicator(getActivity());
        con = new Const();
        editor = getActivity().getSharedPreferences(con.MY_PREFS_NAME, getActivity().MODE_PRIVATE).edit();
        editor.putInt(con.TREAT_NUMBER,0);
        editor.putBoolean(con.FIRST, true);
        editor.putInt(con.TREAT_MENU_TIME, 5);
        editor.putInt(con.INDEX_NUMBER, 0);
        editor.putInt(con.VACUUM_STATE, 1);

        editor.commit();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);//zohar 3000

                    // Do some stuff
                } catch (Exception e) {
                    e.getLocalizedMessage();
                } finally {
                    WelcomeFragment fragment2 = new WelcomeFragment(communicator);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frameLayout, fragment2);
                    fragmentTransaction.commit();
                }
            }
        }).start();
        return v;
    }


}
