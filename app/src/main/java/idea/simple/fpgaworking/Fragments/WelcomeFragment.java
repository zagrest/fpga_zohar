package idea.simple.fpgaworking.Fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import idea.simple.fpgaworking.FPGACommunicator.FPGACommucnicator;
import idea.simple.fpgaworking.R;

/**
 * Created by avivvegh on 9/6/15.
 */
public class WelcomeFragment extends Fragment implements View.OnClickListener {

    ImageView imWhite, imPatientScreen,bSettings,im_small_logo;
    Typeface fontRobo;
    TextView tv_welcome,tv_white_treat,tv_patient_screening;
    View v;
    Context mContext;
    FPGACommucnicator obj;
    public WelcomeFragment(FPGACommucnicator obj){
        this.obj = obj;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         v = inflater.inflate(R.layout.activity_welcome_to, container, false);
        mContext = getActivity();
        fontRobo = Typeface.createFromAsset(getActivity().getAssets(), "fonts/gothic.ttf");


        init();
        return v;

    }

    private void init() {
        im_small_logo = (ImageView) v.findViewById(R.id.im_small_logo);
        im_small_logo.setOnClickListener(this);
        bSettings = (ImageView) v.findViewById(R.id.bSettings);
        bSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getFragmentManager().popBackStack();
                SettingsFragment fragment2 = new SettingsFragment(obj);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frameLayout, fragment2);
                fragmentTransaction.commit();
            }
        });
        tv_patient_screening= (TextView) v.findViewById(R.id.tv_patient_screening);
        tv_patient_screening.setTypeface(fontRobo);
        tv_white_treat = (TextView) v.findViewById(R.id.tv_white_treat);
        tv_white_treat.setTypeface(fontRobo);

        tv_welcome = (TextView) v.findViewById(R.id.tv_welcome);
        tv_welcome.setTypeface(fontRobo);
        imWhite = (ImageView) v.findViewById(R.id.im_product1);
        imPatientScreen = (ImageView) v.findViewById(R.id.im_product2);

        imWhite.setOnClickListener(this);
        imPatientScreen.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.im_small_logo:
                getActivity().getFragmentManager().popBackStack();
                TreatMenuFragment fragment4 = new TreatMenuFragment(obj);
                FragmentManager fragmentManagers = getFragmentManager();
                FragmentTransaction fragmentTransactions = fragmentManagers.beginTransaction();
                fragmentTransactions.replace(R.id.frameLayout, fragment4);
                fragmentTransactions.commit();

                break;
            case (R.id.im_product1):

                //todo: test the first line!
                getActivity().getFragmentManager().popBackStack();

                MixingFragment fragment2 = new MixingFragment(obj);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frameLayout, fragment2);
                fragmentTransaction.commit();
                break;
            case (R.id.im_product2):
                getActivity().getFragmentManager().popBackStack();
                VacuumFragment fragment3 = new VacuumFragment(obj);
                fragmentManager = getFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frameLayout, fragment3);
                fragmentTransaction.commit();
                break;
        }
    }


}
