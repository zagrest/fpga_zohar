package idea.simple.fpgaworking.Fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import idea.simple.fpgaworking.Constance.Const;
import idea.simple.fpgaworking.FPGACommunicator.FPGACommucnicator;
import idea.simple.fpgaworking.Listeners.OnUSBEvenetListener;
import idea.simple.fpgaworking.R;

/**
 * Created by avivvegh on 9/29/15.
 */

public class VacuumFragment extends Fragment implements View.OnClickListener {
    int low;
    int low_low, low_high, high_low,high_high;
    int init_low = 2987,init_hihg= 3121;
    int init_reg_vcuum = 3212;//or 3054
    int mid;
    int high;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    Const con;
    FPGACommucnicator obj;
    Button bVacuumPlus,bVacuumMinus,setVacuumDone;
    TextView bVacuumControl;
    public VacuumFragment(FPGACommucnicator obj){
        this.obj = obj;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.vacuum_layout,container,false);
        con = new Const();
        bVacuumPlus = (Button) v.findViewById(R.id.bSwitch1);
        bVacuumPlus.setOnClickListener(this);
        bVacuumMinus = (Button) v.findViewById(R.id.bSwitch3);
        bVacuumMinus.setOnClickListener(this);
        bVacuumControl = (TextView) v.findViewById(R.id.bVcuumControl);
        bVacuumControl.setOnClickListener(this);
        setVacuumDone = (Button) v.findViewById(R.id.setVacuumDone);
        setVacuumDone.setOnClickListener(this);
        prefs = getActivity().getSharedPreferences(con.MY_PREFS_NAME, getActivity().MODE_PRIVATE);
        editor = getActivity().getSharedPreferences(con.MY_PREFS_NAME, getActivity().MODE_PRIVATE).edit();
        vacuum_state(1);

        return v;
    }

    private void vacuum_state(int vacuum_state) {
        switch(vacuum_state){
            case 0:


//                low = prefs.getInt(con.LOW_LOW,140);
//                high = prefs.getInt(con.LOW_HIGH,12);


                //high !
                init_reg_vcuum +=67;
                high_high = init_reg_vcuum/256;
                high_low = init_reg_vcuum%256;


                //vacuum
                con.setVACUUM_BITHIGH((byte) high_high);
                con.setVACUUM_BITLOW((byte) high_low);
                obj.writeOpCode(con.VACUUM_ON, new OnUSBEvenetListener() {
                    @Override
                    public void onAck() {

                    }

                    @Override
                    public void onDone() {

                    }

                    @Override
                    public void onError() {

                    }

                    @Override
                    public void onProccess() {

                    }
                });




                break;
            case 1:
//                low = prefs.getInt(con.MID_LOW,238);
//                high = prefs.getInt(con.MID_HIGH,11);

                //mid !
                //lock:

                obj.writeOpCode(con.BLADE_LOCK, new OnUSBEvenetListener() {
                    @Override
                    public void onAck() {

                    }

                    @Override
                    public void onDone() {

                    }

                    @Override
                    public void onError() {

                    }

                    @Override
                    public void onProccess() {

                    }
                });
                //wait 7 sec
                try {
                    Thread.sleep(7000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                //valve to treat:
                obj.writeOpCode(con.VALVE, new OnUSBEvenetListener() {
                    @Override
                    public void onAck() {

                    }

                    @Override
                    public void onDone() {

                    }

                    @Override
                    public void onError() {

                    }

                    @Override
                    public void onProccess() {

                    }
                });
                //wait 7 sec

                try {
                    Thread.sleep(7000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                low = prefs.getInt(con.HIGH_LOW,49);
                high = prefs.getInt(con.HIGH_HIGH,12);
                con.setVACUUM_BITHIGH((byte) high);
                con.setVACUUM_BITLOW((byte) low);
//                obj.writeOpSettings(con.VACUUM_ON);
                obj.writeOpCode(con.VACUUM_ON, new OnUSBEvenetListener() {
                    @Override
                    public void onAck() {

                    }

                    @Override
                    public void onDone() {

                    }

                    @Override
                    public void onError() {

                    }

                    @Override
                    public void onProccess() {

                    }
                });

                editor.putInt(con.VACUUM_STATE, 2).commit();

                break;
            case 2:

                //low !
                init_reg_vcuum -=67;
                low_high = init_reg_vcuum/256;
                low_low = init_reg_vcuum%256;
                con.setVACUUM_BITHIGH((byte) low_high);
                con.setVACUUM_BITLOW((byte) low_low);
//                obj.writeOpSettings(con.VACUUM_ON);
                obj.writeOpCode(con.VACUUM_ON, new OnUSBEvenetListener() {
                    @Override
                    public void onAck() {

                    }

                    @Override
                    public void onDone() {

                    }

                    @Override
                    public void onError() {

                    }

                    @Override
                    public void onProccess() {

                    }
                });
//                editor.putInt(con.VACUUM_STATE, 0).commit();

                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bSwitch1:
                //plus
                vacuum_state(2);

                break;
            case R.id.bSwitch3:
                //minus
                vacuum_state(0);

                break;
            case R.id.bVcuumControl:
                //mid
                vacuum_state(1);

                break;
            case R.id.setVacuumDone:


                int low_off = init_reg_vcuum%256;
                int high_off =init_reg_vcuum/256;
                con.setVACUUM_OFF((byte)high_off,(byte)low_off);
                obj.writeOpCode(con.VACUUM_OFF, new OnUSBEvenetListener() {
                    @Override
                    public void onAck() {

                    }

                    @Override
                    public void onDone() {

                    }

                    @Override
                    public void onError() {

                    }

                    @Override
                    public void onProccess() {

                    }
                });


                //


                //valve back
                obj.writeOpSettings(con.VAC_RELEASE);

                //wait 7 sec

                try {
                    Thread.sleep(7000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                //valve treat
                obj.writeOpSettings(con.VALVE);

                try {
                    Thread.sleep(7000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //open lock
                obj.writeOpCode(con.BLADE_OPEN, new OnUSBEvenetListener() {
                    @Override
                    public void onAck() {

                    }

                    @Override
                    public void onDone() {

                    }

                    @Override
                    public void onError() {

                    }

                    @Override
                    public void onProccess() {

                    }
                });


                WelcomeFragment fragment2 = new WelcomeFragment(obj);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frameLayout, fragment2);
                fragmentTransaction.commit();
                break;


        }
    }
}
