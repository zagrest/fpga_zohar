package idea.simple.fpgaworking.utils;

import java.util.Arrays;

/**
 * Created by David on 20-May-15.
 */
public class ByteConvertor {

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public static byte h2b(String s) {
        return (byte) ((Character.digit(s.charAt(0), 16) << 4)
                + Character.digit(s.charAt(1), 16));
    }

    public static String b2h(byte[] bytes){
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02X ", b));
        }
        return sb.toString();
    }

    public static String b2ASCII(byte[] data) {
        StringBuilder sb = new StringBuilder(data.length);
        for (int i = 0; i < data.length; ++ i) {
            if (data[i] < 0) throw new IllegalArgumentException();
            sb.append((char) data[i]);
        }
        return sb.toString();
    }

    /**
     * Converts an int value to a byte array.
     *
     * @param piValueToConvert the original integer
     * @param pbBigEndian true if the bytes are in Big-endian order; false otherwise
     *
     * @return byte[] representation of the int
     */
    public static byte[] intToByteArray(int piValueToConvert, boolean pbBigEndian)
    {
        byte[] aRetVal = new byte[4];

        byte iLowest;
        byte iLow;
        byte iMid;
        byte iHigh;

        iLowest = (byte)(piValueToConvert & 0xFF);
        iLow    = (byte)((piValueToConvert >> 8) & 0xFF);
        iMid    = (byte)((piValueToConvert >> 16) & 0xFF);
        iHigh   = (byte)((piValueToConvert >> 24) & 0xFF);

        if(pbBigEndian)
        {
            aRetVal[3] = iLowest;
            aRetVal[2] = iLow;
            aRetVal[1] = iMid;
            aRetVal[0] = iHigh;
        }
        else
        {
            aRetVal[0] = iLowest;
            aRetVal[1] = iLow;
            aRetVal[2] = iMid;
            aRetVal[3] = iHigh;
        }

        return aRetVal;
    }

    public static void from16_to8(byte[] bytes){

        byte[] bit1 = Arrays.copyOfRange(bytes,0,8);
        byte[] bit2 = Arrays.copyOfRange(bytes,8,16);




    }

}
