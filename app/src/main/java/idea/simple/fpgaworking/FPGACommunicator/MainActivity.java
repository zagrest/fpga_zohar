package idea.simple.fpgaworking.FPGACommunicator;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

import com.ftdi.j2xx.D2xxManager;
import com.ftdi.j2xx.FT_Device;

import idea.simple.fpgaworking.R;

public class MainActivity extends Activity {

    FT_Device ftDev;
    int DevCount = -1;
    public static D2xxManager ftD2xx = null;

    int portIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            ftD2xx = D2xxManager.getInstance(this);
        } catch (D2xxManager.D2xxException e) {
            e.printStackTrace();
        }
        setContentView(R.layout.activity_main);

        if(null == ftDev || false == ftDev.isOpen())
        {
            createDeviceList();
            if(DevCount > 0)
            {
//                connectFunction();
//                setUARTInfoString();
//                setConfig(baudRate, dataBit, stopBit, parity, flowControl);
            }
        }
    }


    void sendData(int numBytes, byte[] buffer)
    {
        if (ftDev.isOpen() == false) {
            Toast.makeText(this, "Device not open!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (numBytes > 0)
        {
            ftDev.write(buffer, numBytes);
        }
    }

    void sendData(byte buffer)
    {
        byte tmpBuf[] = new byte[1];
        tmpBuf[0] = buffer;
        ftDev.write(tmpBuf, 1);
    }

    public void createDeviceList()
    {
        int tempDevCount = ftD2xx.createDeviceInfoList(getApplicationContext());

        if (tempDevCount > 0)
        {
            if( DevCount != tempDevCount )
            {
                DevCount = tempDevCount;
//                updatePortNumberSelector();
            }
        }
        else
        {
            DevCount = -1;
//            currentPortIndex = -1;
        }
    }



}
