package idea.simple.fpgaworking.FPGACommunicator;

import android.app.Application;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.MessageQueue;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.ftdi.j2xx.D2xxManager;
import com.ftdi.j2xx.FT_Device;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

import idea.simple.fpgaworking.Constance.Const;
import idea.simple.fpgaworking.Listeners.OnNewUSBEventListener;
import idea.simple.fpgaworking.Listeners.OnPistonLIster;
import idea.simple.fpgaworking.Listeners.OnUSBEvenetListener;
import idea.simple.fpgaworking.NewFragments.TreatScreenFragment_new_new;
import idea.simple.fpgaworking.R;
//import idea.simple.fpgaworking.android_serialport_api.sample;
//import com.dwin.navy.serialapien.R;
import com.dwin.navy.serialportapi.SerailPortOpt;


/**
 * Created by avivvegh on 9/3/15.
 */
public class FPGACommucnicator_new {

    boolean bSendHexData = false;

    boolean bContentFormatHex = false;

    // show information message while send data by tapping "Write" button in hex content format
    int timesMessageHexFormatWriteData = 0;


    // variables
    final int UI_READ_BUFFER_SIZE = 10240; // Notes: 115K:1440B/100ms, 230k:2880B/100ms
    byte[] writeBuffer;
    byte[] readBuffer;
     public Context global_context;



    // data buffer
    byte[] readDataBuffer; /* circular buffer */
    byte[] readDataBuffer2;

    final int MAX_NUM_BYTES = 65536;


    String readText;
    Const con;
    OnNewUSBEventListener nListener;

//ZOHAR VARS
    boolean COMM_KIND = true; // USB=false,RS232=true
    protected Application mApplication;
    private byte[] mBuffer_232;
    volatile boolean rs232_send_bit = false;
    byte[] readBuffer_232;
    int global_232_size;
    private byte[] mBuffer_485;
    volatile boolean rs485_send_bit = false;
    byte[] readBuffer_485;//
    int global_485_size;
    byte temp_range_par = 0x55;//get it from the gui
    String global_str;

    // modbus_rtu_cmd
    byte Slave_address;
    byte Function_code;
    byte Data[];
    byte DataNum[];

    public FPGACommucnicator_new(Context context, OnNewUSBEventListener nListener) {
        global_context = context;


        this.nListener = nListener;

//        mListener =listenr;
        con = new Const();

        //       Toast.makeText(global_context, "*** ERROR ***", Toast.LENGTH_SHORT).show();

        if (COMM_KIND) {
            readBuffer_232 = new byte[512];
            global_232_size = 0;

            mApplication = new ApplicationSerial();//232 zohar

        }



        // init modem variables

        writeBuffer = new byte[512];
        readBuffer = new byte[UI_READ_BUFFER_SIZE];

        readDataBuffer = new byte[MAX_NUM_BYTES];
        readDataBuffer2 = new byte[6];



    }







    //todo: add the syncronized if its makes problem !!
    public void writeOpSettings(byte[] bytes) {
//        byte[] test = new byte[]{0x01,0x01,0x03,0x1,0x2,0x3};

        appendData(byteToString(bytes, bytes.length, 0));


        int numBytes = bytes.length;


        //						sendData(numBytes, writeBuffer);
        writeBuffer = Arrays.copyOfRange(bytes, 0, bytes.length); //zohar
        if (COMM_KIND) {       //rs232 write zohar
            mBuffer_232 = Arrays.copyOfRange(bytes, 0, bytes.length);
            rs232_send_bit = true;

            //Application.SendMsg232(mBuffer);
        }
    }
    void appendData(String data) {
        if (true == bContentFormatHex) {
            if (timesMessageHexFormatWriteData < 3) {
                timesMessageHexFormatWriteData++;
            }
            return;
        }

        if (true == bSendHexData) {
            SpannableString text = new SpannableString(data);
            text.setSpan(new ForegroundColorSpan(Color.YELLOW), 0, data.length(), 0);
            readText += text;
            bSendHexData = false;
        } else {
            readText += data;
        }

    }


    public String byteToString(byte[] test, int length, int from) {
        String tmp = "";
        if (from == 0) {
            tmp = "write: ";
            for (int i = 0; i < length; i++) {
                if (i < test.length - 1)
                    tmp += test[i] + ",";
                else
                    tmp += test[i];
            }
            tmp += "\n";
        }
        if (from == 1) {
            tmp = "Read: ";
            for (int i = 0; i < length; i++) {
                if (i < test.length - 1)
                    tmp += test[i] + ",";
                else
                    tmp += test[i];
            }
            tmp += "\n";
        }
        if (from == 2) {
            tmp = "";
            for (int i = 0; i < length; i++) {
                if (i < test.length - 1)
                    tmp += test[i] + ",";
                else
                    tmp += test[i];
            }
            tmp += "\n";
        }
        return tmp;
    }






    int globalcount = 0;
    int falsecount = 0;
    int tempcount = 0;
    int mixcount = 0;
    int unident = 0;
    int poscount = 0;
    int vaccount = 0;
    int stopcount = 0;
    int purgecount = 0;
    int treatcount = 0;
    int donecount = 0;
    int errcount = 0;
    int sizecount = 0;
    int bladecount = 0;
    int valcount = 0;
    int intercount =0;





    String hexToAscii(String s) throws IllegalArgumentException {
        int n = s.length();
        StringBuilder sb = new StringBuilder(n / 2);
        for (int i = 0; i < n; i += 2) {
            char a = s.charAt(i);
            char b = s.charAt(i + 1);
            sb.append((char) ((hexToInt(a) << 4) | hexToInt(b)));
        }
        return sb.toString();
    }

    static int hexToInt(char ch) {
        if ('a' <= ch && ch <= 'f') {
            return ch - 'a' + 10;
        }
        if ('A' <= ch && ch <= 'F') {
            return ch - 'A' + 10;
        }
        if ('0' <= ch && ch <= '9') {
            return ch - '0';
        }
        throw new IllegalArgumentException(String.valueOf(ch));
    }

    public final byte calculateLRC(byte[] data, int off, int length, int tailskip) {
        int lrc = 0;
        for (int i = off; i < length - tailskip; i++) {
            lrc += data[i];
        }
        lrc = ~lrc;
        lrc++;
        return (byte) lrc;
    }//calculateLRC
//stub functions
//
void Report_on_Interrupt_Msg(int code,int oper)
{
    //code = 0-mix,oper=1-done
    //code = 0-mix,oper=2-done err

    //code = 1-treat,oper=1-treat done
    //code = 1-treat,oper=2-treat done err


    //code = 2-purge,oper=1-purge done
    //code = 2-purge,oper=2-purge done err

    //code = 3-stop,oper=1-stop done

    //code = 4-vacuum release,oper=1-vacuum release done

    //code = 5-Cartridge ,oper=1-Inserted
    //code = 5-Cartridge ,oper=2-Empty

    //code = 6-Cover ,oper=1-Closed
    //code = 6-Cover ,oper=2-Opened

    //code = 7-MPU ,oper=1-is Connected
    //code = 7-MPU ,oper=2-is NOT Connected

    //code = 8-TEMPRATURE ,oper=Value

    //code = 9-485 exception,oper=value
    //code = 10-485 un exception msg ,oper=value

    //code = 11-reset,oper=1-reset done
    //code = 11-reset,oper=2-reset done err
}

void Report_on_Mix_Cycle(int num)
{


}

void Report_on_Purge_Time(int num)
{


}
    //send cmds
    void Send_Mix_Parm()
    {
        writeOpSettings(con.COUNTA);
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        writeOpSettings(con.GAS_RELEAS);
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //  con.A_MAX = new byte[] {0x01,0x01,0x02,0x03,0x0,(byte) 219};
        writeOpSettings(con.A_MAX);
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //con.B_MAX = new byte[] {0x01,0x01,0x02,0x04,0x0,(byte) 219};
        writeOpSettings(con.B_MAX);
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        writeOpSettings(con.A_B_MIN);
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //con.MIX_TIME = new byte[]{0x01,0x01,0x02,0x07,0x0,0x02};
        writeOpSettings(con.MIX_TIME);

    }

    void Send_Mix()
    {
        byte temp_buf[];
        temp_buf = new byte[6];

        temp_buf[0]=0x22;
        temp_buf[1]=0x01;
        temp_buf[2]=0x01;
        temp_buf[3]=0x01;//on
        temp_buf[4]=0;
        temp_buf[5]=0;
        writeOpSettings(temp_buf);
        //writeOpSettings(con.MIXING);
    }



    void Send_Vaccum_On(byte data1,byte data2)
    {
        byte temp_buf[];
        temp_buf = new byte[6];

        temp_buf[0]=0x33;
        temp_buf[1]=0x01;
        temp_buf[2]=0x06;
        temp_buf[3]=0x01;//on
        temp_buf[4]=data1;
        temp_buf[5]=data2;
        writeOpSettings(temp_buf);
    }

    void Send_Vaccum_Off(byte data1,byte data2)
    {
        byte temp_buf[];
        temp_buf = new byte[6];

        temp_buf[0]=0x33;
        temp_buf[1]=0x01;
        temp_buf[2]=0x06;
        temp_buf[3]=0x0;//off
        temp_buf[4]=data1;
        temp_buf[5]=data2;
        writeOpSettings(temp_buf);
    }

    void Send_Purge()
    {
//   public  byte[] PURGE_ON = {INDEX,0x01,0x06,0x01,10, (byte) 202};
// Purge Command: Message Type: 01, Function: 01, Data0: 06, Data1+Data2: Vacuum Level. Vacuum Off: Data1+Data0 : Value 0

        byte temp_buf[];
        temp_buf = new byte[6];

        temp_buf[0]=0x44;
        temp_buf[1]=0x01;
        temp_buf[2]=0x06;
        temp_buf[3]=0x0;//off
        temp_buf[4]=0;
        temp_buf[5]=0;
        writeOpSettings(temp_buf);
        // writeOpSettings(con.PURGE_ON);
    }



    void Send_Treatment(byte data1,byte data2)
    {
//Treatment Command: Message Type: 01, Function: 01, Data0: 02, Data1+Data2: Gel Quantity
        byte temp_buf[];
        temp_buf = new byte[6];

        temp_buf[0]=0x066;
        temp_buf[1]=0x01;
        temp_buf[2]=0x01;
        temp_buf[3]=0x02;
        temp_buf[4]=data1;
        temp_buf[5]=data2;
        writeOpSettings(temp_buf);
//        writeOpSettings(con.TREATMENT_COUNTINUE);

    }

    void Send_Stop()
    {

        writeOpSettings(con.END_STOP);

    }

    void Send_Temrature_Range_set(String s1,String s2)
    {
        int NUM_OF_ELEM=5;
        byte temp_buf[];
        temp_buf = new byte[6];

        byte temp_buf_func[];
        temp_buf_func = new byte[NUM_OF_ELEM];

        byte temp_buf_data[];
        temp_buf_data = new byte[NUM_OF_ELEM*2];
        byte j1,j2;
        int i=0;
        int n=0;
        int j=0;
        int nn=0;
        String hex_func = "0303030303";
        String hex_data = "00010002000300040005";
        String hex = "010300010001";

        temp_buf_func = hex_func.getBytes();
        temp_buf_data = hex_data.getBytes();
        while (i<NUM_OF_ELEM) {
            Function_code = temp_buf_func[i];
            //temp_buf[0] = ((byte) s0[1]);
            temp_buf[0] = Slave_address;//pos 0
            temp_buf[1] = Function_code;// pos 1
            if (Function_code == 06)
                set485_ack = true;
            Data[0] = temp_buf_data[n];
            Data[1] = temp_buf_data[n + 1];

            temp_buf[2] = Data[0];// pos 4
            temp_buf[3] = Data[1];// pos 5

            temp_buf[4] = DataNum[0];// pos 4 - const
            temp_buf[5] = DataNum[1];// pos 5 - const


            byte LRC = 0;

            char jj;


            LRC = calculateLRC(temp_buf, 0, 6, 0);
            jj = (char) LRC;


            mBuffer_485[0] = 0x3a;//header
            mBuffer_485[1] = 0x30;//slave address
            mBuffer_485[2] = 0x31;//slave address

            char a, b;

            a = hex_func.charAt(i);
            b = hex_func.charAt(i + 1);
            mBuffer_485[3] = (byte) a;//set func
            mBuffer_485[4] = (byte) b;//set func


            a = hex_data.charAt(nn);
            b = hex_data.charAt(nn + 1);
            mBuffer_485[5] = (byte) a;//set data0
            mBuffer_485[6] = (byte) b;//set data0

            a = hex_data.charAt(nn+2);
            b = hex_data.charAt(nn + 3);
            mBuffer_485[7] = (byte) a;//set data1
            mBuffer_485[8] = (byte) b;//set data1

            mBuffer_485[9] = 0x30;//set datanum
            mBuffer_485[10] = 0x30;//set datanum
            mBuffer_485[11] = 0x30;//set datanum
            mBuffer_485[12] = 0x31;//set datanum


            j = (int) (jj & 0x00ff);

            if (j > 15) {
                j2 = (byte) (j % 16);//reminder
                j1 = (byte) ((j - j2) / 16);//high

                if (j1 > 9)
                    a = (char) (j1 + 0x37);
                else a = (char) (j1 + 0x30);

                if (j2 > 9)
                    b = (char) (j2 + 0x37);
                else b = (char) (j2 + 0x30);

            } else {

                a = (char) 0x30;
                if (j > 9)
                    b = (char) (j + 0x37);
                else b = (char) (j + 0x30);
            }

//    LRC = calculateLRC(mBuffer_485, 1, 12, 0);

            mBuffer_485[13] = (byte) a;//insert lrc
            mBuffer_485[14] = (byte) b;//insert lrc

            mBuffer_485[15] = 0x0D;
            mBuffer_485[16] = 0x0A;

            rs485_send_bit = true;//ok to send thread
            //advance ptr
            i++;//index on func
            n = n + 2;//index on data byte
            nn  = nn + 4;//index on data str
        }//end while
        timer_for_temp();//set timer polling of temperature value
    }

    void Stop_Tempratue_Poll()
    {//stop polling temprature
        if (temp_poll != null) {
            temp_poll.cancel();//end timer operation
        }
    }

    void Send_Reset()
    {
        writeOpSettings(con.RESET_MSG);

    }
    void Send_Mask()
    {


    }
    void Send_UnMask()
    {


    }
/*
Treatment Command: Message Type: 01, Function: 01, Data0: 02, Data1+Data2: Gel Quantity

Purge Command: Message Type: 01, Function: 01, Data0: 06, Data1+Data2: Vacuum Level. Vacuum Off: Data1+Data0 : Value 0
 */
    Timer temp_poll;
    void timer_for_temp() {//set timer polling of temperature value


//set timer till stop poll will received
        temp_poll = new Timer();
        temp_poll.schedule(new TimerTask() {
                @Override
                public void run() {


                    send_read_temp_485();//send read req

                }//end run
            },  1000,1);//end schedular
        }//end func

    void send_read_temp_485()
    {//poll temprature value

        mBuffer_485[0] = 0x3a;
        mBuffer_485[1] = 0x30;
        mBuffer_485[2] = 0x31;
        mBuffer_485[3] = 0x30;
        mBuffer_485[4] = 0x33;

        mBuffer_485[5] = 0x30;
        mBuffer_485[6] = 0x30;
        mBuffer_485[7] = 0x30;
        mBuffer_485[8] = 0x31;

        mBuffer_485[9] = 0x30;
        mBuffer_485[10] = 0x30;
        mBuffer_485[11] = 0x30;
        mBuffer_485[12] = 0x31;

        mBuffer_485[13] = 0x46;
        mBuffer_485[14] = 0x41;
        mBuffer_485[15] = 0x0d;
        mBuffer_485[16] = 0x0a;

        rs485_send_bit = true;
    }


    boolean reset_parse()
    {
        //RESET FUNCTION
        if(readDataBuffer2[0]==0x11)
        {
            resetcount++;
            if ((char) readDataBuffer2[1] == 5) {
//                                    nListener.onDone();
                //Toast.makeText(global_context, "*** Treat Done ***", Toast.LENGTH_SHORT).show();
                global_str = "*** Reset Done ***";
                // stop timer - zohar
                if (resetTimer != null) {
                    resetTimer.cancel();//end timer operation
                }
                Log.v("RESET LOG", "RESET:DONE" + " " + "OK");
                done_wait_reset = false;
                Report_on_Interrupt_Msg(11, 1);//inform gui
                return true;
            }
            if ((char) readDataBuffer2[1] == 1) {
//                                    mListener.onAck();
                //Toast.makeText(global_context, "*** Treat Ack ***", Toast.LENGTH_SHORT).show();
                global_str = "*** Reset Ack ***";
                Log.v("TREAT LOG", "RESET:ACK" + " " + "OK");
                // start timer - zohar
                timer_for_reset();
                done_wait_reset = true;

                return true;
            }
        }//end reset
        return false;
    }

    int resetcount=0;
    boolean done_wait_reset = false;
    Timer resetTimer;
    void timer_for_reset()
    {//set timer till mix done will received
        resetTimer = new Timer();
        resetTimer.schedule(new TimerTask() {
            @Override
            public void run() {


                if (resetTimer != null) {
                    resetTimer.cancel();//end timer operation
                }


                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                global_str = "*** Reset Done ERROR - after 5 sec ***";
                Report_on_Interrupt_Msg(11,2);//inform gui
                Display_once(20);//display global_str


            }//end run
        },  5000,1);//end schedular
    }//end func

    boolean new_mix_parse() {

        if (readDataBuffer2[0] == 0x22) {
            mixcount++;
            // the writebuffer in order to make sure it's not the temp
            if ((char) readDataBuffer2[1] == 5) {
                global_str = "*** Mix Done ***";
                done_wait_mix = false;

                if (mixTimer != null) {
                    mixTimer.cancel();//end timer operation
                }
                if (mixShowTimer != null) {
                    mixShowTimer.cancel();//end timer operation
                }
                Report_on_Interrupt_Msg(0, 1);//inform gui
                Log.v("MIX LOG", "MIX:DONE" + " " + "OK");


                return true;
            }
            if ((char) readDataBuffer2[1] == 1) {

                timer_for_mix();//set timer
                timer_for_mixShow();//set timer for process watch


                //Toast.makeText(global_context, "*** Mix Ack ***", Toast.LENGTH_SHORT).show();
                global_str = "*** Mix Ack ***";
                Log.v("MIX LOG", "MIX:ACK" + " " + "OK");
                done_wait_mix = true;

                return true;
            }
        }//end mix
        return false;
    }

    int last_read=99;
    boolean mix_parse() {
        //MIXING FUNCTIOn

        if (readDataBuffer2[1] == 1 && readDataBuffer2[2] == 5 && readDataBuffer2[0] == 0x55) {
            mixcount++;//read ack
            //Read_Mix = new byte[]{0x55, 0x02, 0x05, 0x0, 0x, 0x};
            return true;
        }
        if (readDataBuffer2[1] == 2 && readDataBuffer2[2] == 5 && readDataBuffer2[0] == 0x55) {
            mixcount++;//read val

            Log.v("MIX LOG", "MIX:READ = " + " " + readDataBuffer2[5]);
            if (last_read!= readDataBuffer2[5]) {
                last_read = readDataBuffer2[5];
                Report_on_Mix_Cycle(last_read);//inform gui
                char cycle = (char) (readDataBuffer2[5]+0x30);
                global_str = "MIX:cycle = " + cycle;
                Display_once(2);
            }
            return true;
        }
        if (readDataBuffer2[2] == 3 && writeBuffer[1] == 1) {
            mixcount++;
            // the writebuffer in order to make sure it's not the temp
            if ((char) readDataBuffer2[1] == 5) {
                global_str = "*** Mix Done ***";
                done_wait_mix = false;

                if (mixTimer != null) {
                    mixTimer.cancel();//end timer operation
                }
                if (mixShowTimer != null) {
                    mixShowTimer.cancel();//end timer operation
                }
                Report_on_Interrupt_Msg(0, 1);//inform gui
                Log.v("MIX LOG", "MIX:DONE" + " " + "OK");


                return true;
            }
            if ((char) readDataBuffer2[1] == 1) {

                timer_for_mix();//set timer
                timer_for_mixShow();//set timer for process watch


                //Toast.makeText(global_context, "*** Mix Ack ***", Toast.LENGTH_SHORT).show();
                global_str = "*** Mix Ack ***";
                Log.v("MIX LOG", "MIX:ACK" + " " + "OK");
                done_wait_mix = true;

                return true;
            }
        }//end mix
        if (readDataBuffer2[0] == 1 && readDataBuffer2[1] == 1 && readDataBuffer2[2] == 2) {
            //mix setup response
            mixcount++;
            return true;
        }
        if (readDataBuffer2[0] == 1 && readDataBuffer2[1] == 1 && readDataBuffer2[2] == 0x0A) {
            //mix setup response
            valcount++;
            return true;
        }
        return false;
    }

    Timer mixShowTimer;
    byte[] Read_Mix;
    void timer_for_mixShow()
    {//set timer till mix done will received
        mixShowTimer = new Timer();
        mixShowTimer.schedule(new TimerTask() {
            @Override
            public void run() {

                if (!done_wait_mix) {
                    if (mixShowTimer != null) {
                        mixShowTimer.cancel();//end timer operation
                    }
                }
                else {
                    Read_Mix = new byte[]{0x55, 0x02, 0x05, 0x0, 0x0, 0x0};
                    writeOpSettings(Read_Mix);//read mix state val
                    Log.v("MIX LOG", "MIX:READ" + " " + "w");
                }
            }//end run
        },  4000,1);//end schedular
    }//end func

    Timer mixTimer;
    void timer_for_mix()
    {//set timer till mix done will received
        mixTimer = new Timer();
        mixTimer.schedule(new TimerTask() {
            @Override
            public void run() {


                if (mixTimer != null) {
                    mixTimer.cancel();//end timer operation
                }


                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Report_on_Interrupt_Msg(0,2);//inform gui
                global_str = "*** Mix Done ERROR - after 5 minutes ***";
                Display_once(20);//display global_str



            }//end run
        },  300000,1);//end schedular
    }//end func

    Timer treatTimer;
    void timer_for_treat()
    {//set timer till mix done will received
        treatTimer = new Timer();
        treatTimer.schedule(new TimerTask() {
            @Override
            public void run() {


                if (treatTimer != null) {
                    treatTimer.cancel();//end timer operation
                }


                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                global_str = "*** Treat Done ERROR - after 5 sec ***";
                Report_on_Interrupt_Msg(1,2);//inform gui
                Display_once(20);//display global_str


            }//end run
        },  5000,1);//end schedular
    }//end func

    boolean new_treat_parse()
    {
        //TREATMENT FUNCTION
        if(readDataBuffer2[0]==0x66)
        {
            treatcount++;
            if ((char) readDataBuffer2[1] == 5) {
//                                    nListener.onDone();
                //Toast.makeText(global_context, "*** Treat Done ***", Toast.LENGTH_SHORT).show();
                global_str = "*** Treat Done ***";
                // stop timer - zohar
                if (treatTimer != null) {
                    treatTimer.cancel();//end timer operation
                }
                Log.v("TREAT LOG", "TREAT:DONE" + " " + "OK");
                done_wait_treat = false;
                Report_on_Interrupt_Msg(1, 1);//inform gui
                return true;
            }
            if ((char) readDataBuffer2[1] == 1) {
//                                    mListener.onAck();
                //Toast.makeText(global_context, "*** Treat Ack ***", Toast.LENGTH_SHORT).show();
                global_str = "*** Treat Ack ***";
                Log.v("TREAT LOG", "TREAT:ACK" + " " + "OK");
                // start timer - zohar
                timer_for_treat();
                done_wait_treat = true;

                return true;
            }
        }//end treat
        return false;
    }

boolean treat_parse()
{
    //TREATMENT FUNCTION
    if(readDataBuffer2[2]==1&&readDataBuffer2[3]==2
            &&readDataBuffer2[0]==1)
    {
        treatcount++;
        if ((char) readDataBuffer2[1] == 5) {
//                                    nListener.onDone();
            //Toast.makeText(global_context, "*** Treat Done ***", Toast.LENGTH_SHORT).show();
            global_str = "*** Treat Done ***";
            // stop timer - zohar
            if (treatTimer != null) {
                treatTimer.cancel();//end timer operation
            }
            Log.v("TREAT LOG", "TREAT:DONE" + " " + "OK");
            done_wait_treat = false;
            Report_on_Interrupt_Msg(1, 1);//inform gui
            return true;
        }
        if ((char) readDataBuffer2[1] == 1) {
//                                    mListener.onAck();
            //Toast.makeText(global_context, "*** Treat Ack ***", Toast.LENGTH_SHORT).show();
            global_str = "*** Treat Ack ***";
            Log.v("TREAT LOG", "TREAT:ACK" + " " + "OK");
            // start timer - zohar
            timer_for_treat();
            done_wait_treat = true;

            return true;
        }
    }//end treat
    return false;
}
    boolean new_purge_parse() {
        if (readDataBuffer2[1] == 0x44) {
            purgecount++;
            if ((char) readDataBuffer2[1] == 1) {
                //nListener.onPurgeOnAck();//due to problem with reliability and duplicate actions

                global_str = "*** Purge Ack ***";
                Log.v("PURGE LOG", "PURGE:ACK" + " " + "OK");
                done_wait_purge = true;
                timer_for_purge();//set timer
                timer_for_purgeShow();//set timer for process watch

                return true;
            }
            if ((char) readDataBuffer2[1] == 5) {
                //nListener.onPurgeOnAck();//due to problem with reliability and duplicate actions

                //Toast.makeText(global_context, "*** Purge Ack ***", Toast.LENGTH_SHORT).show();
                global_str = "*** Purge Done ***";
                Log.v("PURGE LOG", "PURGE:DONE" + " " + "OK");
                done_wait_purge = false;
                if (purgeTimer != null) {
                    purgeTimer.cancel();//end timer operation
                }
                if (purgeShowTimer != null) {
                    purgeShowTimer.cancel();//end timer operation
                }
                Report_on_Interrupt_Msg(2,1);//inform gui
                return true;
            }

        }//end purge
        return false;
    }//END FUNC

    int save_last_purge_time=0;
    int last_purge_read=99999;
    boolean purge_parse() {
        //PURGE_ON FUNCTION
        if (readDataBuffer2[1] == 1 && readDataBuffer2[2] == 2 && readDataBuffer2[0] == 0x56) {
            purgecount++;//read ack
            //Read_Purge = new byte[]{0x56, 0x02, 0x02, 0x0, 0x, 0x};
            return true;
        }
        if (readDataBuffer2[1] == 2 && readDataBuffer2[2] == 2 && readDataBuffer2[0] == 0x56) {
            purgecount++;//read val

            Log.v("PURGE LOG", "PURGE:READ = " + " " + readDataBuffer2[3]);
            if (last_purge_read!= readDataBuffer2[3]) {
                last_purge_read = readDataBuffer2[3];
                Report_on_Purge_Time(last_purge_read);//inform gui
                char cycle;
                if (readDataBuffer2[3] < 10) {
                    cycle = (char) (readDataBuffer2[3] + 0x30);
                    global_str = "PURGE:time = " + cycle;
                }
                else
                {
                    char dec = (char) ((readDataBuffer2[3] % 10) + 0x30);
                    cycle = (char) (((readDataBuffer2[3] - (readDataBuffer2[3] % 10)) / 10) + 0x30);
                    global_str = "PURGE:time = " +  cycle + dec;
                }
                if (last_purge_read>0) {
                    if (purgeShowTimer != null) {
                        purgeShowTimer.cancel();//end timer operation - no more read cmd
                    }
                    save_last_purge_time = last_purge_read;//save for the gui
                    last_purge_read = 99999;//for next time
                }
                Display_once(8);
            }
            return true;
        }
        if (readDataBuffer2[2] == 6)
            purgecount++;


        if (Arrays.equals(readDataBuffer2, con.PURGE_ON)) {
            if ((char) readDataBuffer2[1] == 1) {
                //nListener.onPurgeOnAck();//due to problem with reliability and duplicate actions

                 global_str = "*** Purge Ack ***";
                Log.v("PURGE LOG", "PURGE:ACK" + " " + "OK");
                done_wait_purge = true;
                timer_for_purge();//set timer
                timer_for_purgeShow();//set timer for process watch

                return true;
            }
            if ((char) readDataBuffer2[1] == 5) {
                //nListener.onPurgeOnAck();//due to problem with reliability and duplicate actions

                //Toast.makeText(global_context, "*** Purge Ack ***", Toast.LENGTH_SHORT).show();
                global_str = "*** Purge Done ***";
                Log.v("PURGE LOG", "PURGE:DONE" + " " + "OK");
                done_wait_purge = false;
                if (purgeTimer != null) {
                    purgeTimer.cancel();//end timer operation
                }
                if (purgeShowTimer != null) {
                    purgeShowTimer.cancel();//end timer operation
                }
                Report_on_Interrupt_Msg(2,1);//inform gui
                return true;
            }

        }//end purge
    return false;
    }//END FUNC

    Timer purgeShowTimer;
    byte[] Read_Purge;
    void timer_for_purgeShow()
    {//set timer till mix done will received
        purgeShowTimer = new Timer();
        purgeShowTimer.schedule(new TimerTask() {
            @Override
            public void run() {


                if (!done_wait_purge) {
                    if (purgeShowTimer != null) {
                        purgeShowTimer.cancel();//end timer operation
                    }
                }
                else {
                    Read_Purge = new byte[]{0x56, 0x02, 0x02, 0x0, 0x0, 0x0};
                    writeOpSettings(Read_Purge);//read purge state val
                    Log.v("PURGE LOG", "PURGE:READ" + " " + "w");
                }
            }//end run
        },  9000,1);//end schedular
    }//end func

    Timer purgeTimer;
    void timer_for_purge()
    {//set timer till mix done will received
        purgeTimer = new Timer();
        purgeTimer.schedule(new TimerTask() {
            @Override
            public void run() {

                if (purgeShowTimer != null) {
                    purgeShowTimer.cancel();//end timer operation - no more read cmd
                }
                if (purgeTimer != null) {
                    purgeTimer.cancel();//end timer operation
                }


                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                global_str = "*** Purge Done ERROR - after 1 minute ***";
                Display_once(20);//display global_str
                Report_on_Interrupt_Msg(2,2);//inform gui

            }//end run
        },  60000,1);//end schedular
    }//end func





    boolean stop_parse() {
        //END_STOP FUNCTION
        if (readDataBuffer2[2] == 7 && readDataBuffer2[3] == 0
                && readDataBuffer2[4] == 0 && readDataBuffer2[5] == 0) {
            stopcount++;
            if ((char) readDataBuffer2[1] == 5) {
                //Toast.makeText(global_context, "*** Stop Function Done***", Toast.LENGTH_SHORT).show();
                global_str = "*** Stop Function Done ***";
                Log.v("STOP T LOG", "STOP T:DONE" + " " + "OK");
                //nListener.onEndStopAck();
                Report_on_Interrupt_Msg(3,1);//inform gui
                return  true;
            }
            if ((char) readDataBuffer2[1] == 1) {
                //Toast.makeText(global_context, "*** Stop Function Ack***", Toast.LENGTH_SHORT).show();
                global_str = "*** Stop Function Ack ***";
                Log.v("STOP T LOG", "STOP T:ACK" + " " + "OK");
                //nListener.onEndStopAck();
                return true;
            }
        }//end stop
        return false;
    }
    boolean new_vac_parse() {
        //blade func

        //VACUUM_RELEASE FUNCTION
        if (readDataBuffer2[2] == 0x33 && readDataBuffer2[2] == 6 && readDataBuffer2[3] == 0x0) {
            vaccount++;
            if ((char) readDataBuffer2[1] == 5) {
                //Toast.makeText(global_context, "*** Vaccum Release Ack ***", Toast.LENGTH_SHORT).show();
                global_str = "*** Vaccum Release Done ***";
                Log.v("VAC R LOG", "VAC R:DONE" + " " + "OK");
                //nListener.onVacuumReleaseDone();
                Report_on_Interrupt_Msg(4,1);//inform gui

                return true;
            }
            if ((char) readDataBuffer2[1] == 1) {
                //Toast.makeText(global_context, "*** Vaccum Release Ack ***", Toast.LENGTH_SHORT).show();
                global_str = "*** Vaccum Release Ack ***";
                Log.v("VAC R LOG", "VAC R:ACK" + " " + "OK");
                //nListener.onVacuumReleaseDone();
                return true;
            }
        }//end vac

        return false;
    }

    boolean vac_parse() {
        //blade func
        if (readDataBuffer2[1] == 1 && readDataBuffer2[2] == 0x0c && readDataBuffer2[2] == 0x0f0) {
            bladecount++;
            return true;
        }
        //VACUUM_RELEASE FUNCTION
        if (readDataBuffer2[2] == 1 && readDataBuffer2[3] == 0x04) {
            vaccount++;
            if ((char) readDataBuffer2[1] == 5) {
                //Toast.makeText(global_context, "*** Vaccum Release Ack ***", Toast.LENGTH_SHORT).show();
                global_str = "*** Vaccum Release Done ***";
                Log.v("VAC R LOG", "VAC R:DONE" + " " + "OK");
                //nListener.onVacuumReleaseDone();
                Report_on_Interrupt_Msg(4,1);//inform gui

                return true;
            }
            if ((char) readDataBuffer2[1] == 1) {
                //Toast.makeText(global_context, "*** Vaccum Release Ack ***", Toast.LENGTH_SHORT).show();
                global_str = "*** Vaccum Release Ack ***";
                Log.v("VAC R LOG", "VAC R:ACK" + " " + "OK");
                //nListener.onVacuumReleaseDone();
                return true;
            }
        }//end vac

    return false;
    }

boolean pos_parse() {

    if (readDataBuffer2[2] == 0x0A && readDataBuffer2[1] == 1) {
        poscount++;//pos ack
        return true;
    }
       //POSITION FIRST
        if (readDataBuffer2[1] == 2 && readDataBuffer2[2] == 0x0A) {
            poscount++;
            if (con.PISTON_BOOLEAN == false) {
                //nListener.onFirstPistonPositon(readDataBuffer2[4], readDataBuffer2[5]);
                Log.v("POSITION LOG", "POSITION:OK" + " " + readDataBuffer2[2]);


                //Toast.makeText(global_context, "*** Position Ok ***", Toast.LENGTH_SHORT).show();
               // global_str = "*** Position Ok ***";
                return true;

            } else {
                //nListener.onCurrectPiston(readDataBuffer2[4], readDataBuffer2[5]);
                Log.v("POSITION LOG", "POSITION:OK" + " " + readDataBuffer2[2]);
                //Toast.makeText(global_context, "*** Position Ok ***", Toast.LENGTH_SHORT).show();
                //global_str = "*** Position Ok ***";

                return true;
            }
        }//end pos

    return false;

}

 boolean temp_parse()
    {
    //TEMP
        if (readDataBuffer2[0] == 1 && readDataBuffer2[1] == 1 && readDataBuffer2[2] == 3
                && readDataBuffer2[3] == 0 && readDataBuffer2[4] == 0 && readDataBuffer2[5] == 0) {
            // temp ack
            tempcount++;

            return true;
        }

    if (readDataBuffer2[0] == 1 && readDataBuffer2[1] == 2 && readDataBuffer2[2] == 3
            && readDataBuffer2[3] == 0 && readDataBuffer2[4] == 0) {
        // temp info
        tempcount++;

        if ((char) readDataBuffer2[5] == 1) {
            nListener.onTempIndicatorOk();
            Log.v("TEMP LOG", "TEMP:OK" + " " + readDataBuffer2[5]);
            // Toast.makeText(global_context, "*** TEMP:OK ***", Toast.LENGTH_SHORT).show();

            return true;
        }
        if ((char) readDataBuffer2[5] == 0) {
            nListener.onTempIndicatorError();
            Log.v("TEMP LOG", "TEMP:ERROR" + " " + readDataBuffer2[5]);
            //Toast.makeText(global_context, "*** TEMP:ERROR ***", Toast.LENGTH_SHORT).show();
            global_str = "*** TEMP:ERROR ***";
            return true;

        }

    }//end temp
        return false;
}


    boolean Interrupt_parse() {
        //Interrupt FUNCTIOn



        switch(readDataBuffer2[2]) {
            case 1: {
                if (readDataBuffer2[5]==1) {
                    global_str = "*** Message - Cartridge Inserted ***";
                    Report_on_Interrupt_Msg(5, 1);//inform gui
                }
                    else {
                    global_str = "*** Message - Cartridge is Empty ***";
                    Report_on_Interrupt_Msg(5,2);//inform gui

                }
                Log.v("Interrupt LOG"," " + global_str);
                Display_once(5);//display global_str
                return true;
            }
            case 2: {
                if (readDataBuffer2[5]==1) {
                    global_str = "*** Message - Cover Is Closed ***";
                    Report_on_Interrupt_Msg(6, 1);//inform gui
                }
                else {
                    global_str = "*** Message - Cover Is Open ***";
                    Report_on_Interrupt_Msg(6, 2);//inform gui
                }
                Log.v("Interrupt LOG"," " + global_str);
                Display_once(5);//display global_str
                return true;
            }
            case 3: {
                if (readDataBuffer2[5]==1) {
                    global_str = "*** Message - MPU is Connected to HMU ***";
                    Report_on_Interrupt_Msg(7, 1);//inform gui

                }
                else {
                    global_str = "*** Message -  MPU is NOT Connected to HMU ***";
                    Report_on_Interrupt_Msg(7, 2);//inform gui

                }

                Log.v("Interrupt LOG"," " + global_str);
                Display_once(5);//display global_str
                return true;
            }
            default:;
        }//end switch
        return false;
    }//end func


    public synchronized void Display_once(int n) {

        //display the string
      int i=0;
        while(i<n) {
            // create a handler to post messages to the main thread
            Handler mHandler = new Handler(Looper.getMainLooper());
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(global_context, global_str, Toast.LENGTH_SHORT).show();

                }

            });
            i++;
        }
    }

    void checkDone() {
                    //we are in wait state for a done
        if (done_wait_mix)
        {

            done_wait_mix = false;
            global_str = "*** Mix Done Received ***";
            Log.v("DONE LOG"," " + global_str);

            if (mixTimer != null) {
                mixTimer.cancel();//end timer operation
            }
            if (mixShowTimer != null) {
                mixShowTimer.cancel();//end timer operation
            }
            nListener.onMixDone();//just for now zohar
        }
        else if (done_wait_treat)
        {
            done_wait_treat = false;
            global_str = "*** Treat Done Received ***";
            Log.v("DONE LOG"," " + global_str);
            if (treatTimer != null) {
                treatTimer.cancel();//end timer operation
            }
        }
        else if (done_wait_purge)
        {
            done_wait_purge = false;
            global_str = "*** Purge Done Received ***";
            Log.v("DONE LOG"," " + global_str);
            if (purgeTimer != null) {
                purgeTimer.cancel();//end timer operation
        }
        else if (done_wait_reset)
            done_wait_treat = false;
        global_str = "*** Reset Done Received ***";
        Log.v("DONE LOG"," " + global_str);
        if (resetTimer != null) {
            resetTimer.cancel();//end timer operation
        }
    }
    }//end func

    boolean done_wait_mix = false;
    boolean done_wait_treat = false;
    boolean done_wait_purge = false;

        int buf_index = 0;
    public void ParseDataReceived(final byte[] buffer , int size) {
// rs232 data msg parser - zohar
        int i = 0;
        int j = 0;
        boolean loop_bit;


        if (size % 6 != 0) {
            //Toast.makeText(global_context, "*** Unexcepted Size != % 6 at once ignored ***", Toast.LENGTH_SHORT).show();
            global_str = "*** Unexcepted Size != % 6 at once ignored ***";
            Display_once(20);//display global_str
            sizecount++;
            return;
        }


        while (size > 0) {
            loop_bit = true;
            while (loop_bit) {
                j = i + 6;
                for (; i < j; i++)
                    readDataBuffer2[buf_index++] = buffer[i];

                if (buf_index < 6) return;// not enougth data


                //msg received
                buf_index = 0;
                globalcount++;

                if (readDataBuffer2[1] == 3) {
                    //INTERRUPT msg catch zohar

                     readDataBuffer2[1] = 1; // change to ack
                    writeOpSettings(readDataBuffer2);//send ack
                    readDataBuffer2[1] = 3; // change back to interrupt - good order
                    intercount++;
                    Interrupt_parse();
                    break;
                }

                if (readDataBuffer2[1] == 4) {
                    //error catch zohar

                    global_str = "*** ERROR ***";
                    Display_once(20);//display global_str

                    readDataBuffer2[1] = 1; // change to ack
                    writeOpSettings(readDataBuffer2);//send ack
                    readDataBuffer2[1] = 4; // change back to err - good order
                    errcount++;
                    break;
                }

                if (readDataBuffer2[1] == 5) {
                    //error catch zohar
                    global_str = "*** DONE ***";
                    if (done_wait_mix || done_wait_treat || done_wait_purge)
                        checkDone();//we are in wait state for a done
                    Display_once(5);//display global_str

                    //send err ack
                    donecount++;

                }


                if (readDataBuffer2[1] != 1 && readDataBuffer2[1] != 2 && readDataBuffer2[1] != 5) {
                    //unexecepted msg catch zohar ==3
                    global_str = "*** Unexcepted Message ***";
                    Display_once(20);//display global_str
                    //send msg ack
                    //readDataBuffer2[1] = 1; // change to ack
                    //writeOpSettings(readDataBuffer2);//send ack
                    //readDataBuffer2[1] = 3; // change back to err - good order
                    falsecount++;
                    break;
                }


                if (mix_parse()) break;
                if (new_mix_parse()) break;

                if (treat_parse()) break;
                if (new_treat_parse()) break;

                if (purge_parse()) break;
                if (new_purge_parse()) break;

                if (stop_parse()) break;

                if (vac_parse()) break;
                if  (new_vac_parse()) break;

                if (pos_parse()) break;

                if (temp_parse() ) break;

                if (reset_parse()) break;

                unident++;
                loop_bit = false;//quit the while
                }/* end while true */


                if (size > 0) size = size - 6;

            }//end while

            readDataBuffer2 = new byte[]{0x0, 0x0, 0x0, 0x0, 0x0, 0x0};


        }//end function

    byte[] readDataBuffer485;
    int unident485 = 0;
    int buf_index485 = 0;
    int globalcount485 = 0;
    int sizecount485 = 0;
    int falsecount485 = 0;
    int read485_data=0;//get read data
    boolean set485_ack = false;
    int set485count = 0;
    int read485count = 0;



    public void ParseDataReceived485(  int size) {
// rs485 data msg parser - zohar
        int i = 0;
        int j = 0;
        boolean loop_bit;
        int msg_size = 0;

        readDataBuffer485 = new byte[18];//init received buffer

        if (size % 15 != 0 && size % 17 !=0) {
            global_str = "*** Unexcepted Size 485 != % 15 at once ignored ***";
            //check for an exception
            if (size % 11 == 0)
            if ((readBuffer_485[3] ==0x38 && readBuffer_485[4] ==0x36) || (readBuffer_485[3]==0x38 && readBuffer_485[4] ==0x33))
            {
                char code = (char) (readBuffer_485[6] );
                global_str = "*** 485 Exeception Received code =***" + code;
                Report_on_Interrupt_Msg(9, readBuffer_485[6]);//inform gui
            }

            Display_once(20);//display global_str
            sizecount485++;
            return;
        }




        while (size > 0) {
            loop_bit = true;
            while (loop_bit) {

                if (readBuffer_485[4+j] ==0x36)
                    msg_size = 17;//set reply msg is 17
                else if (readBuffer_485[4+j] ==0x33)
                    msg_size = 15;//read reply msg is 15
                else
                {
                    //unexecepted msg catch zohar ==3
                    global_str = "*** Unexcepted Message ***";
                    Report_on_Interrupt_Msg(10, readBuffer_485[4+j]);//inform gui

                    Display_once(20);//display global_str
                    falsecount485++;
                    break;
                }

                j = i + msg_size;
                for (; i < j; i++)
                    readDataBuffer485[buf_index485++] = readBuffer_485[i];

                if (buf_index485 < msg_size) return;// not enougth data


                //msg received
                buf_index485 = 0;
                globalcount485++;

                //read/set  response
                     if (readDataBuffer485[4] == 0x33) {//read response msg
                         byte x1,x2,x3,x4;
                         x1 = readDataBuffer485[7];
                         x2 = readDataBuffer485[8];
                         x3 = readDataBuffer485[9];
                         x4 = readDataBuffer485[10];

                         read485_data = ((x1-0x30)*16*16*16)+((x2-0x30)*16*16)+((x3-0x30)*16)+(x4-0x30);
                         Report_on_Interrupt_Msg(8, read485_data);//inform gui
                         char dec1 = (char) (x1);
                         char dec2 = (char) (x2);
                         char dec3 = (char) (x3);
                         char dec4 = (char) (x4);
                         global_str = "485 read temp res (in hex): = " +  dec1 + dec2 + dec3 + dec4;

                         Display_once(1);
                         read485count++;
                         break;

                     }
                     if (readDataBuffer485[4] == 0x36) {//set response msg
                         global_str = "485 set res ack" + "";
                         Display_once(1);
                         if (set485_ack==true)
                             set485_ack = false;
                         set485count++;
                         break;
                     }


                global_str = "485 unident msg" + "";
                Display_once(2);
                unident485++;
                loop_bit = false;//quit the while
            }/* end while true */


            if (size > 0) size = size - msg_size;

        }//end while

      }//end function

//zohar

    public class ApplicationSerial extends Application {

        private static final String TAG = "ApplicationSerial";
        private SerailPortOpt serialPort;
        private SerailPortOpt serialPort485;

        protected OutputStream mOutputStream;
        private InputStream mInputStream;

        private OutputStream mOutputStream485;
        private InputStream mInputStream485;

        private ReadThread232 mReadThread232;
        private SendThread232 mSendThread232;

        private ReadThread485 mReadThread485;
        private SendThread485 mSendThread485;


        byte ErrorcheckCRC[];

        /** Instance of the current application. */
        public  ApplicationSerial instance;

        /**
         * Constructor.
         */
        public ApplicationSerial() {
            instance = this;
            serialPort = new SerailPortOpt();
            openSerialPort();//232 object
            			/* Create a receiving thread */
            mReadThread232 = new ReadThread232();
            mReadThread232.start();

            /* Create a send thread */
            mBuffer_232 = new byte[6];
            rs232_send_bit = false;

            mSendThread232 = new SendThread232();
            mSendThread232.start();

            serialPort485 = new SerailPortOpt();
            openSerialPort485();//485 obj

                        			/* Create a receiving thread */
            mReadThread485 = new ReadThread485();
            mReadThread485.start();

            /* Create a send thread */
            mBuffer_485 = new byte[17];
            rs485_send_bit = false;

            mSendThread485 = new SendThread485();
            mSendThread485.start();

            start_485_appl();
        }

        /**
         * Gets the application context.
         *
         * @return the application context
         */
        public  Context getContext() {
            if (instance == null) {
                instance = new ApplicationSerial();
            }
            return instance;
        }



        protected void onCreat() {


        }


        protected void onDestroy() {
            // TODO Auto-generated method stub
            Log.i(TAG, "==>onDestroy serial in");
            closeSerialPort();
            serialPort = null;

            closeSerialPort485();
            serialPort485 = null;
            //super.onDestroy();
            Log.i(TAG, "==>onDestroy serial out");
        }

void start_485_appl()
{
    byte i=0;

//    while(true) {

 //       Slave_address = i++;
        Function_code = 0x3;
        DataNum[0]= 0;
        DataNum[1] = 0x1;
        Data[0] = 0;
        Data[1] = 1;//temp_range_par;


        Send_485_cmd();

//        Slave_address = i++;
        Function_code = 0x3;//read
        Data[0] = 0;
        Data[1] = 1;

        //Send_485_cmd();
  //  }
}




void Send_485_cmd()
{
    byte temp_buf[];
    temp_buf = new byte[6];

    byte j1,j2;
    String s1,s2;
    final String[] s0 = {"1","3","0","1","0","1"};//in string [] format
    String hex = "010300010001";

//    temp_buf = hex.getBytes();

    temp_buf[0] = Slave_address;//pos 0
    temp_buf[1] = Function_code;// pos 1
    if (Function_code==06)
        set485_ack = true;


    temp_buf[2] = DataNum[0];// pos 2
    temp_buf[3] = DataNum[1];// pos 3

    temp_buf[4] = Data[0];// pos 4
    temp_buf[5] = Data[1];// pos 5


    byte LRC = 0;

    char jj;



    LRC = calculateLRC(temp_buf, 0, 6, 0);
    jj = (char) LRC;


    mBuffer_485[0] = 0x3a;//header
    char a,b;

//    s2 = hexToAscii(hex);
    int i=0;
    int j=hex.length();
    for (i = 0; i <j; i+=2) {
        a = hex.charAt(i);
        b = hex.charAt(i+1);
        mBuffer_485[i+1]  = (byte) a;
        mBuffer_485[i+2]  = (byte) b;
    }



    j = (int) (jj & 0x00ff);

    if (j > 15)
    {
        j2 = (byte) (j % 16);//reminder
        j1= (byte)((j - j2)/16);//high

        if (j1 > 9)
            a = (char) (j1 + 0x37);
        else a = (char) (j1 +0x30);

        if (j2 > 9)
            b = (char) (j2 + 0x37);
        else b = (char) (j2 +0x30);

    }
    else {

        a = (char) 0x30;
        if (j > 9)
            b = (char) (j + 0x37);
        else b = (char) (j +0x30);
    }

//    LRC = calculateLRC(mBuffer_485, 1, 12, 0);

    mBuffer_485[i + 1] = (byte) a;
    mBuffer_485[i + 2] = (byte) b;

    mBuffer_485[i+3]  = 0x0D;
    mBuffer_485[i+4]  = 0x0A;

    rs485_send_bit = true;//ok to send thread
}//


 void Send_485_test()
 {//


     mBuffer_485[0] = 0x3a;
     mBuffer_485[1] = 0x30;
     mBuffer_485[2] = 0x31;
     mBuffer_485[3] = 0x30;
     mBuffer_485[4] = 0x33;

     mBuffer_485[5] = 0x30;
     mBuffer_485[6] = 0x30;
     mBuffer_485[7] = 0x30;
     mBuffer_485[8] = 0x31;

     mBuffer_485[9] = 0x30;
     mBuffer_485[10] = 0x30;
     mBuffer_485[11] = 0x30;
     mBuffer_485[12] = 0x31;

     mBuffer_485[13] = 0x46;
     mBuffer_485[14] = 0x41;
     mBuffer_485[15] = 0x0d;
     mBuffer_485[16] = 0x0a;

     rs485_send_bit = true;//ok to send thread
     try {
         Thread.sleep(300);
     } catch (InterruptedException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
     }


     mBuffer_485[0] = 0x3a;
     mBuffer_485[1] = 0x30;
     mBuffer_485[2] = 0x31;
     mBuffer_485[3] = 0x30;
     mBuffer_485[4] = 0x36;

     mBuffer_485[5] = 0x30;
     mBuffer_485[6] = 0x30;
     mBuffer_485[7] = 0x30;
     mBuffer_485[8] = 0x31;

     mBuffer_485[9] = 0x30;
     mBuffer_485[10] = 0x30;
     mBuffer_485[11] = 0x36;
     mBuffer_485[12] = 0x34;

     mBuffer_485[13] = 0x39;
     mBuffer_485[14] = 0x34;
     mBuffer_485[15] = 0x0d;
     mBuffer_485[16] = 0x0a;


      rs485_send_bit = true;//ok to send thread

 }

        private void openSerialPort485() {

            serialPort485.mDevNum = "S2";//COM2
            serialPort485.mSpeed = 19200;//19200;
            serialPort485.mDataBits = 7;
            serialPort485.mStopBits = 1;//1
            serialPort485.mParity = 110;//0

            // modbus_rtu_cmd init
            Slave_address = 01;//broadcast address
            Function_code = 0x6;//init set
            Data = new byte[2];//get size
            DataNum = new byte[2];//get size
            ErrorcheckCRC = new byte[2];//get size


            //get read data
            if (serialPort485.mFd == null) {
                if ((serialPort485.openDev(serialPort485.mDevNum))!=null) {
                    Log.i("uart 485 port opened", "");

                    serialPort485.setSpeed(serialPort485.mFd, serialPort485.mSpeed);
                    Log.i("uart port 485 operate", "Mainactivity.java==>uart set speed..."
                            + serialPort485.mSpeed);
                    serialPort485.setParity(serialPort485.mFd, serialPort485.mDataBits,
                            serialPort485.mStopBits, serialPort485.mParity);
                    Log.i("uart port 485 operate",
                            "Mainactivity.java==>uart other params..."
                                    + serialPort485.mDataBits + "..."
                                    + serialPort485.mStopBits + "..." + serialPort485.mParity);

                    mInputStream485 = serialPort485.getInputStream();
                    mOutputStream485 = serialPort485.getOutputStream();


                }
            }
        }

        private void openSerialPort() {

            serialPort.mDevNum = "S0";//COM0
            serialPort.mSpeed = 57600;
            serialPort.mDataBits = 8;
            serialPort.mStopBits = 1;
            serialPort.mParity = 110;//0

            if (serialPort.mFd == null) {
                serialPort.openDev(serialPort.mDevNum);

                Log.i("uart port operate", "Mainactivity.java==>uart open");
                serialPort.setSpeed(serialPort.mFd, serialPort.mSpeed);
                Log.i("uart port operate", "Mainactivity.java==>uart set speed..."
                        + serialPort.mSpeed);
                serialPort.setParity(serialPort.mFd, serialPort.mDataBits,
                        serialPort.mStopBits, serialPort.mParity);
                Log.i("uart port operate",
                        "Mainactivity.java==>uart other params..."
                                + serialPort.mDataBits + "..."
                                + serialPort.mStopBits + "..." + serialPort.mParity);

                mInputStream = serialPort.getInputStream();
                mOutputStream = serialPort.getOutputStream();
            }
        }

        private void closeSerialPort485() {

            if (mReadThread485 != null) {
                mReadThread485.interrupt();
                // mReadThread = null;
            }

            if (mSendThread485 != null) {
                mSendThread485.interrupt();
                // mReadThread = null;
            }

            if (serialPort485.mFd != null) {
                Log.i("uart port operate", "Mainactivity.java==>uart 485 stop");
                serialPort485.closeDev(serialPort485.mFd);
                Log.i("uart port operate", "Mainactivity.java==>uart 485 stoped");
            }
        }


        private void closeSerialPort() {

            if (mReadThread232 != null) {
                mReadThread232.interrupt();
                // mReadThread = null;
            }

            if (mSendThread232 != null) {
                mSendThread232.interrupt();
                // mReadThread = null;
            }

            if (serialPort.mFd != null) {
                Log.i("uart port operate", "Mainactivity.java==>uart stop");
                serialPort.closeDev(serialPort.mFd);
                Log.i("uart port operate", "Mainactivity.java==>uart stoped");
            }
        }


        private class ReadThread485 extends Thread {
            byte[] buf = new byte[512];

            @Override
            public void run() {
                int size=0;
                super.run();
                Log.v(TAG, "ReadThread485 start" + " ");
                while (!isInterrupted()) {

                    if (mInputStream485 != null) {
                        size = serialPort485.readBytes(buf);
                       //mInputStream485 = null;

                        if (size > 0) {
                            // new String(buf, 0, size).getBytes()»áÔì³ÉÊý¾Ý´íÎó


                            Log.v(TAG, "ReadThread485 received data len = " + size);

                            readBuffer_485 = Arrays.copyOfRange(buf, 0, size);
                            global_485_size = size;
                            ParseDataReceived485(size);//zohar - send to parser


                        }
                    }
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }//end while
            }
        }//end thread 485

        private class ReadThread232 extends Thread {
            byte[] buf = new byte[512];

            @Override
            public void run() {
                super.run();
                Log.i(TAG, "ReadThread232==>buffer:" + buf.length);
                while (!isInterrupted()) {
                    int size;
                    if (mInputStream == null)
                        return;
                    size = serialPort.readBytes(buf);

                    if (size > 0) {
                        // new String(buf, 0, size).getBytes()»áÔì³ÉÊý¾Ý´íÎó



                        System.arraycopy(buf, 0, readBuffer_232, 0, size);
                        global_232_size = size;
                        ParseDataReceived(readBuffer_232, size);//zohar - send to parser


                    }
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }//end while
            }
        }//end thread

        public class SendThread485 extends Thread{

            @Override
            public void run() {
                // TODO Auto-generated method stub
                Log.i(TAG, "SendThread485 start");
                super.run();
                while (true) {

                    if(null != serialPort485.mFd && rs485_send_bit){
                        //Log.i(TAG, src);
                        serialPort485.writeBytes(mBuffer_485);
                        rs485_send_bit = false;
                        Log.v(TAG, "SendThread485 sends data  " + "");
                    }
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }


            }
        }//end thread 485


        public class SendThread232 extends Thread{

            @Override
            public void run() {
                // TODO Auto-generated method stub
                Log.i(TAG, "SendThread232==>run");
                super.run();
                while (true) {

                       if(null != serialPort.mFd && rs232_send_bit){
                            //Log.i(TAG, src);
                            serialPort.writeBytes(mBuffer_232);
                            rs232_send_bit = false;
                        }
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }


            }
        }//end thread


    }

}
