package idea.simple.fpgaworking.Comboseekbar;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;

import idea.simple.fpgaworking.R;


/**
 * Created by avivvegh on 8/26/15.
 */
public class CustomThumbDrawable extends Drawable {
    /**
     * paints.
     */
    private Paint circlePaint;
    private Context mContext;
    private float mRadius;
    private Bitmap bit;
    private int position;
    private int size;


    public CustomThumbDrawable(Context context, int color,int size) {
        mContext = context;
        mRadius = toPix(15);
        setColor(color);
        this.size = size;
    }

    public void setColor(int color) {
        circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
       // circlePaint.setColor((0xA0 << 24) + (color & 0x00FFFFFF));
        invalidateSelf();
    }

    public float getRadius() {
        return mRadius;
    }

    @Override
    protected final boolean onStateChange(int[] state) {
        invalidateSelf();
        return false;
    }

    @Override
    public final boolean isStateful() {
        return true;
    }

    @Override
    public final void draw(Canvas canvas) {
        int height = this.getBounds().centerY();
        int width = (int) ((double)this.getBounds().centerX()/1.5);
//        canvas.drawCircle(width + mRadius, height, mRadius, circlePaint);
        switch (position){
            case 0:
                bit = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.treatlow);
                canvas.drawBitmap(bit, width,mRadius,circlePaint);
                break;
            case 1:
                bit = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.treatmid);
                canvas.drawBitmap(bit, width,mRadius,circlePaint);
                break;
            case 2:
                bit = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.treathigh);
                canvas.drawBitmap(bit, width,mRadius,circlePaint);
                break;
        }


//        canvas.rotate(270,height,width);

    }

    @Override
    public int getIntrinsicHeight() {
        return (int) (mRadius * 1.5);
    }

    @Override
    public int getIntrinsicWidth() {
        return (int) (mRadius * 1.5);
    }

    @Override
    public final int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }

    @Override
    public void setAlpha(int alpha) {
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
    }

    private float toPix(int size) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, size,
                mContext.getResources().getDisplayMetrics());
    }

    public void setPostion(int pos){
        position = pos;
    }
}